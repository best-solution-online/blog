class ApiUrl{
    //static BaseUrl = "http://localhost/blogbackend/";
    static BaseUrl = "https://blogbackend.bestsolution.me/";
    static photoUrl = this.BaseUrl+'storage/app/';

    static LoginAuth = this.BaseUrl+'auth/login'
    static UserInfo = this.BaseUrl+'user/info'
    static UserAll = this.BaseUrl+'user/all'
    static UserAllSearch = this.BaseUrl+'user/all/search'
    static UserInfoProfile = this.BaseUrl+'user/public'
    static ProfileAuth = this.BaseUrl+'auth/profile'
    static ProfileUpdate = this.BaseUrl+'user/update'
    static ProfileSocialUpdate = this.BaseUrl+'user/social/update'
    static ProfileStatusUpdate = this.BaseUrl+'user/status/update'
    static ProfileStatusUpdateByAdmin = this.BaseUrl+'user/status/update/admin'
    static UserDelete = this.BaseUrl+'user/delete'


    // Category
    static CategoryOne = this.BaseUrl+'category/one'
    static CategorySlug = this.BaseUrl+'category/slug'
    static CategoryAll = this.BaseUrl+'category/all'
    static CategoryCreate = this.BaseUrl+'category/create'
    static CategoryUpdate = this.BaseUrl+'category/update'
    static CategoryDelete = this.BaseUrl+'category/delete'

    // Post
    static PostOne = this.BaseUrl+'post/byid'
    static PostSlug = this.BaseUrl+'post/byslug'
    static PostTrend = this.BaseUrl+'post/trend'
    static PostPopular = this.BaseUrl+'post/popular'
    static PostSearch = this.BaseUrl+'post/search'
    static PostMySearch = this.BaseUrl+'post/mysearch'
    static PostUserAll = this.BaseUrl+'post/all/user'
    static PostAll = this.BaseUrl+'post/all'
    static PostCreate = this.BaseUrl+'post/create'
    static PostUpdate = this.BaseUrl+'post/update'
    static PostDelete = this.BaseUrl+'post/delete'

    //Comment
    static CommentOne = this.BaseUrl+'comment/one'
    static CommentAll = this.BaseUrl+'comment/all'
    static CommentCreate = this.BaseUrl+'comment/create'
    static CommentUpdate = this.BaseUrl+'comment/update'
    static CommentDelete = this.BaseUrl+'comment/delete'

    //Reply
    static ReplyOne = this.BaseUrl+'reply/one'
    static ReplyAll = this.BaseUrl+'reply/all'
    static ReplyCreate = this.BaseUrl+'reply/create'
    static ReplyUpdate = this.BaseUrl+'reply/update'
    static ReplyDelete = this.BaseUrl+'reply/delete'

    //Subscribe
    static FollowCreate = this.BaseUrl+'follow/create'
    static FollowCheck = this.BaseUrl+'follow/check'
    static FollowMy = this.BaseUrl+'follow/my'

    //Like Dislike
    static PostLike = this.BaseUrl+'post/like'
    static PostDislike = this.BaseUrl+'post/dislike'
    static PostLikeDislikeCheck = this.BaseUrl+'post/like/dislike/check'

    //Report
    static ReportCreate = this.BaseUrl+'report/create'
    static ReportDelete = this.BaseUrl+'report/delete'
    static ReportAll = this.BaseUrl+'report/all'

    //Favorite
    static FavoriteCreate = this.BaseUrl+'favorite/create'
    static FavoriteCheck = this.BaseUrl+'favorite/check'
    static FavoriteMy = this.BaseUrl+'favorite/my'
    static FavoriteDelete = this.BaseUrl+'favorite/delete'

    //Ads
    static AdsCreate = this.BaseUrl+'ads/create'
    static AdsUpdate = this.BaseUrl+'ads/update'
    static AdsView = this.BaseUrl+'ads/view'
    static AdsDelete = this.BaseUrl+'ads/delete'
    static AdsMy = this.BaseUrl+'ads/my'
    static AdsAdmin = this.BaseUrl+'ads/admin'
    static AdsOne = this.BaseUrl+'ads/one'
    static AdsClick = this.BaseUrl+'ads/click'

    //Ads Settings Admin
    static AdsSettingOne = this.BaseUrl+'ads/setting/one'
    static AdsSettingAll = this.BaseUrl+'ads/setting/all'
    static AdsSettingCreate = this.BaseUrl+'ads/setting/create'
    static AdsSettingUpdate = this.BaseUrl+'ads/setting/update'
    static AdsSettingDelete = this.BaseUrl+'ads/setting/delete'

    //Settings
    static SettingsUpdate = this.BaseUrl+'settings/update'
    static SettingsOne = this.BaseUrl+'settings/one'





}

export default ApiUrl;
