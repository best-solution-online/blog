import React, {Component} from 'react';
import {DataContext} from "../../context/ContextApi";
import TopBar from "../topbar/TopBar";
import SideBar from "../sidebar/SideBar";
import {ToastContainer} from "react-toastify";

class ContentMain extends Component {
    static contextType = DataContext;
    render() {
        const {MainContentState} = this.context;
        return (
            <>
                <TopBar/>
                <SideBar/>
                <div className={MainContentState}>
                    <ToastContainer position="top-right" autoClose={5000} hideProgressBar={false} newestOnTop={false} closeOnClick
                                    rtl={false} pauseOnFocusLoss draggable pauseOnHover/>
                    {this.props.children}
                </div>
            </>
        );
    }
}

export default ContentMain;
