import React, {Component} from 'react';
import ContentMain from "../content/ContentMain";

class Loading extends Component {
    render() {
        return (
            <div className="container text-center" style={{paddingTop:'200px'}}>
                <img style={{height:'50px'}}  src="/loading.svg"/>
            </div>
        );
    }
}

export default Loading;
