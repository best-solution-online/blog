import React, {Component} from 'react';
import Link from "next/link";
import axios from "axios";
import ApiUrl from "../AppApiUrl/ApiUrl";
import Router from "next/router";
import {DataContext} from "../../context/ContextApi";
import {ToastContainer} from "react-toastify";


class ProfileTop extends Component {
    static contextType = DataContext;
    constructor() {
        super();
        this.state={
            path:'',name:'',email:'',img:'',cover:'',follow:'',token:'',loading:true
        }
    }

    componentDidMount() {
        console.log(Router.pathname)
        this.setState({path:Router.pathname})
        const token = localStorage.token
        if(token){
            this.setState({token:token})
            const myFormData = new FormData()
            myFormData.append('token',token)
            const config = {
                headers : {'content-type':'multipart/form-data'}
            }
            axios.post(ApiUrl.ProfileAuth,myFormData,config)
                .then(res=>{
                    if(res.data.success){
                        this.setState({name:res.data.info[0]['name'],img:res.data.info[0]['img'],
                            cover:res.data.info[0]['cover'],follow:res.data.info[0]['follow'],
                            email:res.data.info[0]['email'],role:res.data.info[0]['role'],token:token,loading:false})
                    }else{
                        console.log("Error")
                       // this.setState({name:'',img:'',
                            //email:'',role:'',token:''})
                        //localStorage.removeItem('token')
                    }
                })
                .catch(error=>{
                    //localStorage.removeItem('token')
                    console.log(error)
                })
        }else{
            Router.push('/')
        }
    }

    render() {

        const {img,cover,follow,name} = this.context
            return (
                <>
                    {cover ? <img src={ApiUrl.photoUrl+cover} className="cover" /> :
                        <img className="cover" src='/cover.png'/>
                    }
                    <div className="row cover_row" >
                        <div className="col-md-6 col-12 ">
                            <div className="row mt-4">
                                <div className="col-md-3 col-3">
                                    {img ? <img className="acc_img" src={ApiUrl.photoUrl+img}/>:
                                        <img className="acc_img" src="/profile.png" />
                                    }
                                </div>
                                <div className="col-md-7 col-7 mt-1">
                                    <h3 style={{marginBottom:'0px'}}>{name}</h3>
                                    <p >{follow} Followers</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-12 p-md-5 p-3">
                             <Link href='/settings'><a className="btn btn-primary" style={{marginRight:'10px'}}>CUSTOMIZE PROFILE</a></Link>
                            <Link href='/post'><a className="btn btn-primary">MANAGE POST</a></Link>
                        </div>
                        <div className="col-md-12 col-12 acc_list">
                            <Link href="/account" ><a className={this.state.path=="/account"?"active":''}>  HOME </a></Link>
                            <Link href="/account/favorite" ><a className={this.state.path=="/account/favorite"?"active":''}>  FAVORITE </a></Link>
                            <Link href="/account/follows" ><a className={this.state.path=="/account/follows"?"active":''}>  FOLLOWS </a></Link>
                            <Link href="/account/about" ><a className={this.state.path=="/account/about"?"active":''}>  ABOUT </a></Link>
                        </div>
                    </div>
                    <ToastContainer position="top-right" autoClose={5000} hideProgressBar={false} newestOnTop={false} closeOnClick
                                    rtl={false} pauseOnFocusLoss draggable pauseOnHover/>
                </>
            );

    }
}

export default ProfileTop;