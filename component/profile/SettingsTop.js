import React, {Component} from 'react';
import {DataContext} from "../../context/ContextApi";
import Router from "next/router";
import axios from "axios";
import ApiUrl from "../AppApiUrl/ApiUrl";
import Link from "next/link";
import Head from "next/head";
import {ToastContainer} from "react-toastify";

class SettingsTop extends Component {
    static contextType = DataContext;
    constructor() {
        super();
        this.state={
            path:'',name:'',email:'',img:'',cover:'',follow:'',token:'',loading:true
        }
    }

    componentDidMount() {
        console.log(Router.pathname)
        this.setState({path:Router.pathname})
        const token = localStorage.token
        if(token){
            this.setState({token:token})
            const myFormData = new FormData()
            myFormData.append('token',token)
            const config = {
                headers : {'content-type':'multipart/form-data'}
            }
            axios.post(ApiUrl.ProfileAuth,myFormData,config)
                .then(res=>{
                    if(res.data.success){
                        this.setState({name:res.data.info[0]['name'],img:res.data.info[0]['img'],
                            cover:res.data.info[0]['cover'],follow:res.data.info[0]['follow'],
                            email:res.data.info[0]['email'],role:res.data.info[0]['role'],token:token,loading:false})
                    }else{
                        //this.setState({name:'',img:'',
                            //email:'',role:'',token:''})
                        //localStorage.removeItem('token')
                        console.log("Error")
                    }
                })
                .catch(error=>{
                    //localStorage.removeItem('token')
                    console.log(error)
                })
        }else{
            Router.push('/')
        }
    }

    render() {

        const {img,cover,follow,name} = this.context
        return (
            <>
                <Head>
                    <title> Settings And Customize Your account </title>
                </Head>
                <div className="row cover_row" >
                    <div className="col-md-6 col-12 ">
                        <div className="row mt-4">
                            <div className="col-md-3 col-3">
                                {img ? <img className="acc_img" src={ApiUrl.photoUrl+img}/>:
                                    <img className="acc_img" src="/profile.png" />
                                }
                            </div>
                            <div className="col-md-7 col-7 mt-3">
                                <h3 style={{marginBottom:'0px'}}>{name}</h3>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12 col-12 acc_list mt-3">
                        <Link href="/settings" ><a className={this.state.path=="/settings"?"active":''}>  Basic Info </a></Link>
                        <Link href="/settings/social" ><a className={this.state.path=="/settings/social"?"active":''}>  Social Links </a></Link>
                        <Link href="/settings/permission" ><a className={this.state.path=="/settings/permission"?"active":''}>  Permission </a></Link>
                    </div>
                </div>
                <ToastContainer position="top-right" autoClose={5000} hideProgressBar={false} newestOnTop={false} closeOnClick
                                rtl={false} pauseOnFocusLoss draggable pauseOnHover/>
            </>
        );

    }
}

export default SettingsTop;