import React, {Component} from 'react';
import Link from "next/link";
import {DataContext} from "../../context/ContextApi";
import Router from "next/router";
import ApiUrl from "../AppApiUrl/ApiUrl";

class SideBar extends Component {
    static contextType = DataContext;
    constructor() {
        super();
        this.state={
            path:''
        }
    }

    componentDidMount() {
        console.log(Router.pathname)
        this.setState({path:Router.pathname})
    }

    render() {
        const {SideNavState,OnMenuButtonClick,logo,fb,youtube,twitter,instagram,linkedin,footer} = this.context;
        return (
            <div className={SideNavState}>

                <div className="sideLogo">
                    <i className="fa fa-bars" onClick={OnMenuButtonClick} style={{color:'#030303',fontSize:'18px'}}></i>
                    <Link href="/">
                        {logo ? <img src={ApiUrl.photoUrl+logo} className="sidelogoimg" />:''}
                    </Link>
                </div>

                <div className="sideMenuItem">
                    <ul>
                        <li className={this.state.path=="/"?"active":''}>
                            <Link href="/" >
                                <a> <i className="fa fa-home"></i> Home </a>
                            </Link>
                        </li>
                        <li className={this.state.path=="/trending"?"active":''}>
                            <Link href="/trending" >
                                <a> <i className="fa fa-rocket"></i> Trending </a>
                            </Link>
                        </li>

                        <li className={this.state.path=="/popular"?"active":''}>
                            <Link href="/popular" >
                                <a> <i className="fa fa-heart"></i> Popular </a>
                            </Link>
                        </li>

                        <li className={this.state.path=="/category"?"active":''} >
                            <Link href="/category" >
                                <a> <i className="fa fa-tag"></i> Category </a>
                            </Link>
                        </li>
                         <hr/>
                        {/*Second List style={{borderBottom:'1px solid #dedcdc'}} */}

                        <li className={this.state.path=="/about"?"active":''}>
                            <Link href="/about" >
                                <a> <i className="fa fa-address-card"></i> About  </a>
                            </Link>
                        </li>
                        <li className={this.state.path=="/contact"?"active":''} >
                            <Link href="/contact" >
                                <a> <i className="fa fa-envelope"></i> Contact US </a>
                            </Link>
                        </li>

                        <hr/>

                        {/* Third List */}

                        <li >
                            <a href={fb} target="blank"> <i className="fab fa-facebook"></i> Facebook </a>
                        </li>
                        <li >
                            <a href={youtube} target="blank"> <i className="fab fa-youtube"></i> Youtube </a>
                        </li>

                        <li >
                            <a href={twitter} target="blank"> <i className="fab fa-twitter"></i> Twitter </a>
                        </li>

                        <li>
                            <a href={instagram} target="blank"> <i className="fab fa-instagram"></i> Instagram </a>
                        </li>
                        <li  >
                            <a href={linkedin} target="blank"> <i className="fab fa-linkedin"></i> Linkedin </a>
                        </li>
                        <hr/>
                        <p className="sidebottom">
                            <Link href="/about">About</Link> <Link href="/contact">Contact US </Link>
                            <Link href="/terms">Terms</Link> <Link href="/privacy">Privary</Link>
                            <Link href="/ads">Advertisement</Link>
                            <p className="mt-3 mb-3">@{footer}</p>
                            <br/>
                        </p>
                    </ul>
                </div>

            </div>
        );
    }
}

export default SideBar;
