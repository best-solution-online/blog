import React, {Component} from 'react';
import Head from "next/head";
import Link from "next/link";
import Router from "next/router";

class TopBar extends Component {
    constructor() {
        super();
        this.state={
            SideNavState:"sideNavOpen",
            MainContentState:'contentClose',
            ContentOverState:"ContentOverlayClose",
            path:''
        }
    }


    componentDidMount() {
        console.log(Router.pathname)
        this.setState({path:Router.pathname})
    }

    ContentOverlayClickHandler=()=>{
        this.OnMenuButtonClick();
    }

    OnMenuButtonClick(){
        let SideNavState= this.state.SideNavState;
        let ContentOverState= this.state.ContentOverState;
        if(SideNavState==="sideNavOpen"){
            this.setState({SideNavState:"sideNavClose",ContentOverState:"ContentOverlayOpen",MainContentState:"contentOpen"})
        }
        else{
            this.setState({SideNavState:"sideNavOpen",ContentOverState:"ContentOverlayClose",MainContentState:"contentClose"})
        }
    }

    render() {

        return (
             <>
                 <Head>
                     <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"/>
                     <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
                     <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"
                             integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG"
                             crossOrigin="anonymous"></script>
                     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"
                             integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc"
                             crossOrigin="anonymous"></script>
                 </Head>
                 <div className="topmenu">
                     <div className="row">
                         <div className="col-md-3 col-lg-3 col-2 logo">
                             <a>
                                 <i className="fa fa-bars" onClick={this.OnMenuButtonClick.bind(this)} style={{color:'#030303',fontSize:'18px'}}></i>
                             </a>
                             <Link href="/">
                                 <img src='https://cdn.worldvectorlogo.com/logos/youtube-icon.svg' className="logoimg" />
                             </Link>
                         </div>


                         <div className="col-md-6 col-lg-6 col-8 search">
                             <div className="input-group mb-3 ssection">
                                 <input type="text" className="form-control" placeholder=" Search" style={{height:'30px'}}
                                        aria-label="Recipient's username" aria-describedby="basic-addon2"/>
                                     <div className="input-group-append">
                                         <button style={{height:'30px'}}
                                                 className="btn btn-light searchbtn" title="Search" type="button"><i className="fa fa-search"></i>
                                         </button>
                                     </div>
                             </div>
                         </div>

                         <div className="col-md-3 col-lg-3 col-2 menuright">
                             <Link href="/"><a><i className="fa fa-camera" title="Create Post"></i></a></Link>
                             <i className="fa fa-bell"></i>
                             <img src="https://lh3.googleusercontent.com/ogw/ADGmqu9WBXwqvzQTsHZE3aTn7cgDj_F6rSmKIHnP2gLG5g=s32-c-mo" className="profilemenu" />
                         </div>
                     </div>
                 </div>
                 <div style={{height:'56px'}}></div>

                 <div onClick={this.ContentOverlayClickHandler}  className={this.state.ContentOverState}></div>


                 <div className={this.state.SideNavState}>

                     <div className="sideLogo">
                        <i className="fa fa-bars" onClick={this.OnMenuButtonClick.bind(this)} style={{color:'#030303',fontSize:'18px'}}></i>
                        <Link href="/">
                            <img src='https://cdn.worldvectorlogo.com/logos/youtube-icon.svg' className="sidelogoimg" />
                        </Link>
                    </div>


                     <div className="sideMenuItem">
                         <ul>
                             <li className={this.state.path=="/"?"active":''}>
                                 <Link href="/" >
                                     <a> <i className="fa fa-home"></i> Home </a>
                                 </Link>
                             </li>
                             <li className={this.state.path=="/trending"?"active":''}>
                                 <Link href="/trending" >
                                     <a> <i className="fa fa-rocket"></i> Trending </a>
                                 </Link>
                             </li>

                             <li className={this.state.path=="/popular"?"active":''}>
                                 <Link href="/popular" >
                                     <a> <i className="fa fa-heart"></i> Popular </a>
                                 </Link>
                             </li>

                             <li className={this.state.path=="/category"?"active":''} style={{borderBottom:'1px solid #dedcdc'}}>
                                 <Link href="/category" >
                                     <a> <i className="fa fa-tag"></i> Category </a>
                                 </Link>
                             </li>

                             {/*Second List */}

                             <li className={this.state.path=="/about"?"active":''}>
                                 <Link href="/about" >
                                     <a> <i className="fa fa-address-card"></i> About  </a>
                                 </Link>
                             </li>
                             <li className={this.state.path=="/contact"?"active":''} style={{borderBottom:'1px solid #dedcdc'}}>
                                 <Link href="/contact" >
                                     <a> <i className="fa fa-envelope"></i> Contact US </a>
                                 </Link>
                             </li>


                             {/* Third List */}

                             <li >
                                 <a href="https://www.youtube.com/"> <i className="fab fa-facebook"></i> Facebook </a>
                             </li>
                             <li >
                                 <a href="https://www.youtube.com/"> <i className="fab fa-youtube"></i> Youtube </a>
                             </li>

                             <li >
                                 <a href="https://www.youtube.com/"> <i className="fab fa-twitter"></i> Twitter </a>
                             </li>

                             <li>
                                 <a href="https://www.youtube.com/"> <i className="fab fa-instagram"></i> Instagram </a>
                             </li>
                             <li  style={{borderBottom:'1px solid #dedcdc'}}>
                                 <a href="https://www.youtube.com/"> <i className="fab fa-linkedin"></i> Linkedin </a>
                             </li>
                             <p className="sidebottom">
                                 <Link href="/about">About</Link> <Link href="/contact">Contact US </Link>
                                 <Link href="/terms">Terms</Link> <Link href="/privacy">Privary</Link>
                                 <Link href="/ads">Advertisement</Link>
                                 <p className="mt-3">@Best Solution Online</p>
                             </p>
                         </ul>
                     </div>

                 </div>
                 <div className={this.state.MainContentState}>
                     {this.props.children}
                 </div>

             </>
        );
    }
}

export default TopBar;
