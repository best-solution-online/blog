import React, {Component} from 'react';
import Head from "next/head";
import Link from "next/link";
import ApiUrl from "../AppApiUrl/ApiUrl";
import Router from "next/router";
import Axios from 'axios'
import {DataContext} from "../../context/ContextApi";
import GoogleLogin from 'react-google-login';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



class TopBar extends Component {
    static contextType = DataContext;

    constructor() {
        super();
        this.state={
            pDropDown:"pdropdownmenuClose",
            notify:'notify_close',
            OverState:'OverlayClose',
            OverState2:'OverlayClose'
        }
    }

    OverlayState=()=>{
        this.ProfileButtonClick();
    }
    OverlayState2=()=>{
        this.NotifyButtonClick();
    }
    ProfileButtonClick=()=>{
        let pDropDown= this.state.pDropDown;
        if(pDropDown==="pdropdownmenuOpen"){
            this.setState({pDropDown:'pdropdownmenuClose',OverState:'OverlayClose'})
        }
        else{
            this.setState({pDropDown:'pdropdownmenuOpen',OverState:'OverlayOpen'})
        }
    }

    NotifyButtonClick=()=>{
        let nDropDown= this.state.notify;
        if(nDropDown==="notify_open"){
            this.setState({notify:'notify_close',OverState2:'OverlayClose'})
        }
        else{
            this.setState({notify:'notify_open',OverState2:'OverlayOpen'})
        }
    }

    responseGoogle = (response) => {
        const {token,onLogIn} = this.context;
        console.log(response.profileObj)
        const formData = new FormData()
        formData.append('name',response.profileObj.name)
        formData.append('email',response.profileObj.email)
        formData.append('googleId',response.profileObj.googleId)
        Axios.post(ApiUrl.LoginAuth,formData)
            .then(res=>{
                if(res.data.success){
                    //localStorage.setItem('token', res.data.token)
                    onLogIn(res.data.token)
                    toast.success(res.data.success, {
                        position: "bottom-right",
                        autoClose: 5000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    setTimeout(
                        () => Router.push(`/`),
                        3000
                    )
                }
                if(res.data.error){
                    toast.error(res.data.error, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }

            })
            .catch(erorr=>{
                console.log(erorr)
            })
        /*866669576048-7kfhgruri6odgqjm0slke90qqp6tsa59.apps.googleusercontent.com
        main domain
        *
        * 155439141425-vngt5kb6ll0js9elr84oj5rofnohmm9e.apps.googleusercontent.com
        Localhost
        * */
        //console.log(response)
        //alert(response.profileObj.name)
    }
    render() {
      
        const {ContentOverState,OnMenuButtonClick,ContentOverlayClickHandler,onLogOut,svalue,onSearchSubmit,onSearch,token,
            img,name,email,role,logo,meta,tags,des,icon,title,footer,fb} = this.context;
        return (
             <>
                 <Head>
                     <meta name="google-site-verification" content="VCrPNsoQM3KLoUDIW7e1LkMUHVTLQ6R1zrf-_LeWpBI" />
                     <meta property="og:type" content="article" />
                     <meta property="og:title" content={title} />
                     <meta property="og:description" content={des} />
                     <meta property="og:site_name" content={footer} />
                     <meta property="og:image" content={ApiUrl.photoUrl+logo} />
                     <meta property="fb:admins" content={fb} />
                     <meta property="og:image:secure_url" content={ApiUrl.photoUrl+logo} />
                     <meta property="article:tag" content={title} />
                     <meta name="twitter:card" content="summary_large_image" />
                     <meta name="twitter:title" content={title} />
                     <meta name="twitter:description" content={des} />
                     <meta name="twitter:image" content={ApiUrl.photoUrl+icon} />
                     <meta name="description" content={des} />
                     <meta name="keywords" content={tags}/>
                     <meta name="author" content={title}/>

                     <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"/>
                     <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
                     <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                             integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                             crossOrigin="anonymous"></script>
                     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
                             integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
                             crossOrigin="anonymous"></script>
                     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
                             integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
                             crossOrigin="anonymous"></script>
                     {icon ? <link rel="icon" href={ApiUrl.photoUrl+icon} /> :''}
                     <title>{title}</title>
                 </Head>
                 <div className="topmenu">
                     <div className="row">
                         <div className="col-md-3 col-lg-3 col-2 logo">
                             <a>
                                 <i className="fa fa-bars" onClick={OnMenuButtonClick} style={{color:'#030303',fontSize:'18px'}}></i>
                             </a>
                             <Link href="/">
                                 {logo ? <img src={ApiUrl.photoUrl+logo}
                                              className="logoimg" /> :''}
                             </Link>
                         </div>

                         <div className="col-md-6 col-lg-6 col-8 search">
                             <div className="input-group mb-3 ssection">
                                 <input onChange={onSearch} type="text" className="form-control" placeholder=" Search" style={{height:'30px'}}
                                      value={svalue}  aria-label="Recipient's username" aria-describedby="basic-addon2"/>
                                     <div className="input-group-append">
                                         <button onClick={onSearchSubmit} style={{height:'30px'}}
                                                 className="btn btn-light searchbtn" title="Search" type="button"><i className="fa fa-search"></i>
                                         </button>
                                     </div>
                             </div>
                         </div>

                         <div className="col-md-3 col-lg-3 col-2 menuright">
                             {token !=""?
                                 <>
                             <Link href="/post/create"><a><i className="fa fa-camera" title="Create Post"></i></a></Link>

                             <a type="button" onClick={this.NotifyButtonClick} title="Notification">
                                 <span className="count"> 50 </span><i className="fa fa-bell"></i>
                             </a>
                             <div className="pmenu">
                                 {img ? <a onClick={this.ProfileButtonClick}>
                                     <img src={ApiUrl.photoUrl+img}
                                          className="profilemenu" />
                                 </a>:
                                     <a onClick={this.ProfileButtonClick}>
                                         <img src='/profile.png'
                                              className="profilemenu" />
                                     </a>
                                 }
                             </div>

                             </> :
                                 <GoogleLogin
                                     clientId="866669576048-7kfhgruri6odgqjm0slke90qqp6tsa59.apps.googleusercontent.com"
                                     render={renderProps => (
                                         <button className="btn signbtn" onClick={renderProps.onClick} disabled={renderProps.disabled}>
                                             <i className="fa fa-user"></i> <span>SIGN IN </span>
                                         </button>
                                     )}
                                     onSuccess={this.responseGoogle}
                                     onFailure={this.responseGoogle}
                                     cookiePolicy={'single_host_origin'}
                                 />
                             }


                         </div>

                     </div>
                 </div>

                 <div style={{height:'56px'}}></div>

                 <div className={this.state.notify}>
                     <div className="dropdown_head_desk">
                             <div><h5>Notification</h5></div>
                     </div>
                     <div className="dropdown_head">
                         <div className="row">
                             <div className="col-2"><a onClick={this.NotifyButtonClick}><i
                                 className="fas fa-arrow-left"></i></a></div>
                             <div className="col-10"><h4>Notification</h4></div>
                         </div>
                     </div>

                 </div>

                 {token !="" ?  <div className={this.state.pDropDown}>
                     <div className="dropdown_head">
                         <div className="row">
                             <div className="col-2"><a onClick={this.ProfileButtonClick}><i className="fa fa-times"></i></a></div>
                             <div className="col-10"><h4>Account</h4></div>
                         </div>
                     </div>
                     <div className="ptop">
                         <div className="row">
                             <div className="col-md-2 col-2">
                                 {img ?<img className="ppic" src={ApiUrl.photoUrl+img}/>:
                                     <img className="ppic" src="/profile.png"/>
                                 }

                             </div>
                             <div className="col-md-10  col-10 ">
                                 <h6 style={{marginBottom:'0px'}}>{name}</h6>
                                 <p >{email}</p>
                             </div>
                         </div>
                     </div>
                     <div className="pMenuItem">
                         <ul>

                             <li >
                                 <Link href="/account" >
                                     <a> <i className="fa fa-user"></i> Your Account  </a>
                                 </Link>
                             </li>
                             <li >
                                 <Link href="/post/create" >
                                     <a> <i className="fa fa-camera"></i> Create </a>
                                 </Link>
                             </li>

                             <li >
                                 <Link href="/post" >
                                     <a> <i className="fa fa-play"></i>  Content </a>
                                 </Link>
                             </li>
                             <li >
                                 <Link href="/favorite" >
                                     <a> <i className="fa fa-clock"></i> Favorite </a>
                                 </Link>
                             </li>
                             <li >
                                 <Link href="/settings" >
                                     <a> <i className="fa fa-cog"></i> Settings </a>
                                 </Link>
                             </li>
                             <li >
                                 <Link href="/advertising" >
                                     <a> <i className="fa fa-chart-bar"></i> Ads Campain </a>
                                 </Link>
                             </li>
                             <li  >
                                 <a type="button" onClick={onLogOut}> <i className="fa fa-sign-out"></i> Sign out </a>
                             </li>
                              <hr/>

                             {role=="Admin" ? <>
                                 <li >
                                     <Link href="/admin" >
                                         <a> <i className="fa fa-cog"></i> Admin Dashboard </a>
                                     </Link>
                                 </li>
                                 <li >
                                     <Link href="/admin/user" >
                                         <a> <i className="fa fa-users"></i> All User List </a>
                                     </Link>
                                 </li>
                                 <li >
                                     <Link href="/admin/category" >
                                         <a> <i className="fa fa-tag"></i> Category List</a>
                                     </Link>
                                 </li>
                                 <li >
                                     <Link href="/admin/post" >
                                         <a> <i className="fa fa-edit"></i> Post List </a>
                                     </Link>
                                 </li>
                                 <li >
                                     <Link href="/admin/report" >
                                         <a> <i className="fa fa-book"></i> Report List </a>
                                     </Link>
                                 </li>
                                 <li >
                                     <Link href="/admin/ads" >
                                         <a> <i className="fa fa-chart-bar"></i> Ads List </a>
                                     </Link>
                                 </li>
                                 <li >
                                     <Link href="/admin/ads/settings" >
                                         <a> <i className="fa fa-chart-bar"></i> Ads Configure </a>
                                     </Link>
                                 </li>
                             </> :''}

                         </ul>
                     </div>
                 </div> : ""}

                 <ToastContainer position="top-right" autoClose={5000} hideProgressBar={false} newestOnTop={false} closeOnClick
                   rtl={false} pauseOnFocusLoss draggable pauseOnHover/> 
                 <div onClick={ContentOverlayClickHandler}  className={ContentOverState}></div>
                 <div onClick={this.OverlayState}  className={this.state.OverState}></div>
                 <div onClick={this.OverlayState2}  className={this.state.OverState2}></div>

             </>
        );
    }
}

export default TopBar;
