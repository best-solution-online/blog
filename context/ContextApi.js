import React, { Component } from 'react'
import Router from "next/router";
import axios from "axios";
import ApiUrl from "../component/AppApiUrl/ApiUrl";


export const DataContext = React.createContext();

export default class DataProvider extends Component {

    state = {
            SideNavState:"sideNavOpen",
            MainContentState:'contentClose',
            ContentOverState:"ContentOverlayClose",
            search:[],svalue:'',token:'',img:'',name:'',email:'',role:'',cover:'',follow:'',
            logo:'',icon:'',title:'',footer:'',tags:'',meta:'',des:'',fb:'',youtube:'',twitter:'',
            instagram:'',linkedin:'',about:'',contact:'',terms:'',privacy:'',ads:'',ppost:[],pfollow:[],
            pfavorite:[],pinfo:[]
    };



    componentDidMount() {

        axios.post(ApiUrl.SettingsOne)
            .then(res=>{
                this.setState({logo:res.data[0]['logo'],icon:res.data[0]['icon'],
                    title:res.data[0]['title'],footer:res.data[0]['footer'],tags:res.data[0]['tags'],
                    meta:res.data[0]['meta'],des:res.data[0]['des'],fb:res.data[0]['fb'],
                    youtube:res.data[0]['youtube'],twitter:res.data[0]['twitter'],instagram:res.data[0]['instagram'],
                    linkedin:res.data[0]['linkedin'],about:res.data[0]['about'],contact:res.data[0]['contact'],
                    terms:res.data[0]['terms'],privacy:res.data[0]['privacy'],ads:res.data[0]['ads']
                })
            })
            .catch(error=>{
                console.log(error)
            })

        const token = localStorage.token
        if(token){
            this.setState({token:token})
            const myFormData = new FormData()
            myFormData.append('token',token)
            const config = {
                headers : {'content-type':'multipart/form-data'}
            }
            axios.post(ApiUrl.ProfileAuth,myFormData,config)
                .then(res=>{
                    if(res.data.success){
                        this.setState({name:res.data.info[0]['name'],img:res.data.info[0]['img'],
                            cover:res.data.info[0]['cover'],follow:res.data.info[0]['follow'],
                            email:res.data.info[0]['email'],role:res.data.info[0]['role'],token:token})
                    }else{
                        this.setState({name:'',img:'',
                            email:'',role:'',token:''})
                        localStorage.removeItem('token')
                    }
                })
                .catch(error=>{
                    localStorage.removeItem('token')
                    console.log(error)
                })
        }
    }

    onLogIn=(token)=>{
        this.setState({token:token})
        localStorage.setItem('token',token)
        const myFormData = new FormData()
        myFormData.append('token',token)
        const config = {
            headers : {'content-type':'multipart/form-data'}
        }
        axios.post(ApiUrl.ProfileAuth,myFormData,config)
            .then(res=>{
                this.setState({name:res.data.info[0]['name'],img:res.data.info[0]['img'],
                    cover:res.data.info[0]['cover'],follow:res.data.info[0]['follow'],
                    email:res.data.info[0]['email'],role:res.data.info[0]['role'],token:token})
            })
            .catch(error=>{
                localStorage.removeItem('token')
                console.log(error)
                Router.push('/')
            })
    }

    onLogOut=()=>{
        localStorage.removeItem('token')
        this.setState({token:'',name:'',email:'',img:'',role:''})
        Router.push('/')
    }

    ContentOverlayClickHandler=()=>{
        this.OnMenuButtonClick();
    }

    OnMenuButtonClick=()=>{
        let SideNavState= this.state.SideNavState;
        let ContentOverState= this.state.ContentOverState;
        if(SideNavState==="sideNavOpen"){
            this.setState({SideNavState:"sideNavClose",ContentOverState:"ContentOverlayOpen",MainContentState:"contentOpen"})
        }
        else{
            this.setState({SideNavState:"sideNavOpen",ContentOverState:"ContentOverlayClose",MainContentState:"contentClose"})
        }
    }

    onSearch=(e)=>{
        this.setState({svalue:e.target.value})
    }

    onSearchSubmit=()=>{
        if(this.state.svalue !==""){
            const FormS = new FormData()
            FormS.append('name',this.state.svalue)
            axios.post(ApiUrl.PostSearch,FormS)
                .then(res=>{
                    console.log(res)
                    this.setState({search:res.data})
                    Router.push('/results?search_query='+this.state.svalue)
                })
                .catch(error=>{
                    //localStorage.removeItem('token')
                    console.log(error)
                })
        }
    }

    adsClick=(id)=>{
        const FormS = new FormData()
        FormS.append('id',id)
        axios.post(ApiUrl.AdsClick,FormS)
            .then(res=>{
                console.log(res)
            })
            .catch(error=>{
                console.log(error)
            })
    }



    render() {
        const {SideNavState,MainContentState,ContentOverState,search,svalue,token,img,name,email,role,follow,cover,
            logo,icon,title,footer, meta,tags,des,fb,youtube,twitter,instagram,linkedin,about,contact,terms,privacy,ads,
        } = this.state;
        const {OnMenuButtonClick,ContentOverlayClickHandler,onSearch,onSearchSubmit,onLogIn,onLogOut,adsClick} = this;
        return (
            <DataContext.Provider
                value={{SideNavState,MainContentState,ContentOverState,OnMenuButtonClick,ContentOverlayClickHandler,
                    search,svalue,onSearchSubmit,onSearch,token,onLogIn,onLogOut,img,name,email,role,follow,cover,
                    logo,footer, icon,title,meta,des,tags,fb,youtube,twitter,instagram,linkedin,about,contact,terms,privacy,ads,adsClick,

                }}>
                {this.props.children}
            </DataContext.Provider>
        )
    }
}
