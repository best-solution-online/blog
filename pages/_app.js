import '../css/bootstrap.css'
import '../css/style.css'
import '../css/responsive.css'
import DataProvider from "../context/ContextApi";


export default function MyApp({ Component, pageProps }) {
    return <DataProvider>
        <Component {...pageProps} />
    </DataProvider>
}
