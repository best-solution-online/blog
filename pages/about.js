import React, {Component} from 'react';
import Head from "next/head";
import ContentMain from "../component/content/ContentMain";
import {DataContext} from "../context/ContextApi";

class About extends Component {
    static contextType = DataContext;
    render() {
        const {about,title} = this.context;
        return (
            <ContentMain>
                <Head>
                    <title>About Us {title}</title>
                </Head>
                <div className="container">
                    <div className="row m-2 mt-3">
                        <div className="col-md-12 p-md-5 p-col-3  ">
                            <h3 className="text-center mb-3 mt-2">About US</h3>
                            {about}
                        </div>
                    </div>
                </div>
            </ContentMain>
        );
    }
}

export default About;
