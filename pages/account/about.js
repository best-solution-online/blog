import React, {Component} from 'react';
import ContentMain from "../../component/content/ContentMain";
import Head from "next/head";
import ProfileTop from "../../component/profile/ProfileTop";
import Loading from "../../component/loading/Loading";
import axios from "axios";
import ApiUrl from "../../component/AppApiUrl/ApiUrl";
import Router from "next/router";
import Link from "next/link";
import moment from "moment";
import jwtDecode from 'jwt-decode'

class About extends Component {
    constructor() {
        super();
        this.state={
            f_status:'',s_status:'',created_at:'',facebook:'',des:'',post:[],
            twitter:'',youtube:'',instagram:'',linkedin:'',followers:'0',myid:'',
            token:'',loading:true
        }
    }
    componentDidMount() {
        const token = localStorage.token
        if(token){
            const decoded = jwtDecode(token)
            this.setState({token:token,myid:decoded.uid})
            const myFormData = new FormData()
            myFormData.append('token',token)
            myFormData.append('uid',decoded.uid)
            const config = {
                headers : {'content-type':'multipart/form-data'}
            }
            axios.post(ApiUrl.PostUserAll,myFormData,config)
                .then(res=>{
                    this.setState({post:res.data})
                })
                .catch(error=>{
                    console.log(error)
                })
            axios.post(ApiUrl.UserInfo,myFormData)
                .then(res=>{
                    this.setState({
                        f_status:res.data[0]['f_status'],s_status:res.data[0]['s_status'],created_at:res.data[0]['created_at'],
                        facebook:res.data[0]['facebook'],instagram:res.data[0]['instagram'],linkedin:res.data[0]['linkedin'],
                        des:res.data[0]['des'],email:res.data[0]['email'],
                        twitter:res.data[0]['twitter'],youtube:res.data[0]['youtube'],loading:false
                        })
                })
                .catch(error=>{
                    console.log(error)
                })
        }else{
            Router.push('/')
        }
    }


    render() {
        var view = 0;
        const post = this.state.post.map(res=>{
            return view+=parseFloat(res.views)
        })

        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Head>
                        <title> Account </title>
                    </Head>
                    <ProfileTop/>
                    <Loading/>
                </ContentMain>
            );
        }else{
            return (
                <ContentMain>
                    <Head>
                        <title> Account </title>
                    </Head>
                    <ProfileTop/>
                    <div className="container-fluid">
                        <div className="row post_row" >
                            <div className="col-md-8 col-12 pt-5 pr-3">
                                <h5>Description</h5>
                                <p>{this.state.des}</p>
                                <hr style={{width:'100%'}}/>
                                    <div className="about">
                                        <h5>Details</h5>
                                        {this.state.email ? <p>Email : {this.state.email}</p>:''}

                                        {this.state.facebook ? <a href={this.state.facebook} target="blank">
                                            <i className="fab fa-facebook text-primary"></i>
                                        </a> :''}
                                        {this.state.twitter ? <a href={this.state.twitter} target="blank">
                                            <i className="fab fa-twitter text-primary"></i>
                                        </a> :''}
                                        {this.state.youtube ? <a href={this.state.youtube} target="blank">
                                            <i className="fab fa-youtube text-danger"></i>
                                        </a> :''}
                                        {this.state.instagram ? <a href={this.state.instagram} target="blank">
                                            <i className="fab fa-instagram text-primary"></i>
                                        </a> :''}
                                        {this.state.linkedin ? <a href={this.state.linkedin} target="blank">
                                            <i className="fab fa-linkedin text-primary"></i>
                                        </a> :''}
                                        <br/>
                                        <br/>
                                        <hr style={{width:'100%'}}/>
                                    </div>

                            </div>
                            <div className="col-md-4 col-12 pt-5">
                                <h5>Stats</h5>
                                <hr style={{width:'100%'}} />
                                <p>Joined {moment(this.state.created_at).format("MMM Do YY")} </p>
                                <hr style={{width:'100%'}} />
                                <p>{view} views</p>
                                <hr style={{width:'100%'}} />
                            </div>
                        </div>
                    </div>
                </ContentMain>
            );
        }


    }
}

export default About;