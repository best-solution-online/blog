import React, {Component} from 'react';
import Head from "next/head";
import ContentMain from "../../component/content/ContentMain";
import ProfileTop from "../../component/profile/ProfileTop";
import axios from "axios";
import ApiUrl from "../../component/AppApiUrl/ApiUrl";
import Router from "next/router";
import Link from "next/link";
import moment from "moment";
import Loading from "../../component/loading/Loading";

class Favorite extends Component {
    constructor() {
        super();
        this.state={
            favorite:[],token:'',loading:true
        }
    }
    componentDidMount() {
        const token = localStorage.token
        if(token){
            this.setState({token:token})
            const myFormData = new FormData()
            myFormData.append('token',token)
            const config = {
                headers : {'content-type':'multipart/form-data'}
            }
            axios.post(ApiUrl.FavoriteMy,myFormData,config)
                .then(res=>{
                    this.setState({favorite:res.data,loading:false})
                })
                .catch(error=>{
                    console.log(error)
                })
        }else{
            Router.push('/')
        }
    }

    render() {
        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Head>
                        <title> Account </title>
                    </Head>
                    <ProfileTop/>
                    <Loading/>
                </ContentMain>
            );
        }else{
            return (
                <ContentMain>
                    <Head>
                        <title> Account </title>
                    </Head>
                    <ProfileTop/>
                    <div className="container-fluid">

                        <div className="row post_row" >

                            { this.state.favorite.length>0?
                                this.state.favorite.map(res=>{
                                    return(
                                        <>
                                            <div className="col-md-4 col-12 text-dark  pt-3">
                                                <Link href={'/watch?p='+res.slug}>
                                                    <a>{res.img ? <img style={{height:'152px',width:'100%'}}
                                                                       src={ApiUrl.photoUrl+res.img}/>:
                                                        <img style={{height:'152px',width:'100%'}}
                                                             src="/noimage.png"/>
                                                    }</a>
                                                </Link>

                                            </div>
                                            <div className="col-md-8">
                                                <div className="row">
                                                    <div className="col-md-12 col-12">
                                                        <Link href={'/watch?p='+res.slug}>
                                                            <a><h6 className="mt-3 ptitle ">{res.title.substring(0,100)}...</h6></a>
                                                        </Link>
                                                        <Link href={'/watch?p='+res.slug}>
                                                            <a><p className=" views">{res.views} views. {moment(res.created_at, "YYYYMMDD").fromNow()}</p></a>
                                                        </Link>
                                                        <Link href={"/channel/"+res.u_slug}>
                                                            <a><p className="" style={{marginBottom:'0'}}>{res.u_img ?

                                                                <img style={{height:'30px',width:'30px',borderRadius:'50%',marginRight:'5px'}}
                                                                     src={ApiUrl.photoUrl+res.u_img}/>:
                                                                <img style={{height:'30px',width:'30px',borderRadius:'50%',marginRight:'5px'}}
                                                                     src="/profile.png"/>}{res.u_name}</p></a>
                                                        </Link>
                                                        <Link href={'/watch?p='+res.slug}>
                                                            <a><p className="ptitle">{res.body.substring(0,150)}...</p></a>
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </>
                                    )
                                }) :
                                <h6 className="text-center p-5"> Noting </h6>
                            }

                        </div>
                    </div>
                </ContentMain>
            );
        }


    }
}

export default Favorite;