import React, {Component} from 'react';
import Head from "next/head";
import ContentMain from "../../component/content/ContentMain";
import ProfileTop from "../../component/profile/ProfileTop";
import axios from "axios";
import ApiUrl from "../../component/AppApiUrl/ApiUrl";
import Router from "next/router";
import Link from "next/link";
import moment from "moment";
import Loading from "../../component/loading/Loading";

class Follows extends Component {
    constructor() {
        super();
        this.state={
            followData:[],token:'',loading:true
        }
    }
    componentDidMount() {
        const token = localStorage.token
        if(token){
            this.setState({token:token})
            const myFormData = new FormData()
            myFormData.append('token',token)
            const config = {
                headers : {'content-type':'multipart/form-data'}
            }
            axios.post(ApiUrl.FollowMy,myFormData,config)
                .then(res=>{
                    this.setState({followData:res.data,loading:false})
                })
                .catch(error=>{
                    console.log(error)
                })
        }else{
            Router.push('/')
        }
    }

    onFollowChannel=(channel)=>{
        const token = this.state.token;
        if(token == ""){
            toast.error("Login is required", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }else{
            const formData2 = new FormData()
            formData2.append('channel',channel)
            formData2.append('token',token)
            axios.post(ApiUrl.FollowCreate, formData2)
                .then(res=>{
                    //this.componentDidMount()
                    console.log(res)
                    if(res.data.success){
                        console.log(res.data.success)
                        this.setState({followers:res.data.success})
                        this.componentDidMount()
                    }else{
                        console.log("Error Follow")
                    }
                })
                .catch(error=>{
                    console.log(error)
                })
        }
    }

    render() {
        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Head>
                        <title> Account </title>
                    </Head>
                    <ProfileTop/>
                    <Loading/>
                </ContentMain>
            );
        }else{
            return (
                <ContentMain>
                    <Head>
                        <title> Account </title>
                    </Head>
                    <ProfileTop/>
                    <div className="container-fluid">
                        <div className="row post_row" >
                            {this.state.followData.length>0 ? this.state.followData.map(res=>{
                                return(
                                    <>
                                        <div className="col-md-2 col-6 text-dark followuser  pt-3">
                                            <Link href={'/channel/'+res.u_slug}>
                                                <a>{res.u_img ? <img src={ApiUrl.photoUrl+res.u_img}/>:
                                                    <img src="/profile.png"/>
                                                }</a>
                                            </Link>
                                            <Link href={'/channel/'+res.u_slug}>
                                                <a>
                                                    <h6>{res.u_name}</h6>
                                                    <p >{res.f_status =="1"?res.u_followers:'Hidden '} Followers</p>
                                                </a>
                                            </Link>
                                             <button onClick={this.onFollowChannel.bind(this,res.channel_id)}
                                                     className="btn btn-white followbtn">Followed</button>
                                        </div>

                                    </>
                                )
                            }) :<h6 className="text-center p-5"> Noting follows</h6>}
                        </div>
                    </div>
                </ContentMain>
            );
        }


    }
}

export default Follows;