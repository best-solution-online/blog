import React, {Component} from 'react';
import jwtDecode from "jwt-decode";
import Router from "next/router";
import axios from "axios";
import ApiUrl from "../../../../component/AppApiUrl/ApiUrl";
import {toast} from "react-toastify";
import ContentMain from "../../../../component/content/ContentMain";
import Head from "next/head";
import Loading from "../../../../component/loading/Loading";

export async function getServerSideProps({query}) {
    return {
        props: {query}
    }
}

class Id extends Component {
    constructor() {
        super();
        this.state={
            token:'',rate:'',des:'',title:'',acc:'',id:'',loading:true

        }
    }

    componentDidMount() {
        const token = localStorage.token
        if(token){
            const decoded = jwtDecode(token)
            if(decoded.role=="Admin"){
                this.setState({token:token})
                const formData = new FormData()
                formData.append('token',token)
                formData.append('id',this.props.query.id)
                axios.post(ApiUrl.AdsSettingOne,formData)
                    .then(res=>{
                        this.setState({id:res.data[0]['id'],des:res.data[0]['des'],
                            title:res.data[0]['title'],acc:res.data[0]['acc'],rate:res.data[0]['ads_limit'],loading:false})
                    })
                    .catch(error=>{
                        console.log(error)
                    })
            }else{
                Router.push('/')
            }
        }else{
            Router.push('/')
        }
    }

    rate=(e)=>{
        this.setState({rate:e.target.value})
    }
    acc=(e)=>{
        this.setState({acc:e.target.value})
    }
    title=(e)=>{
        this.setState({title:e.target.value})
    }
    des=(e)=>{
        this.setState({des:e.target.value})
    }




    FormSubmit=(event)=>{
        const formData = new FormData()
        formData.append('bank',this.state.bank)
        formData.append('des',this.state.des)
        formData.append('ads_limit',this.state.rate)
        formData.append('title',this.state.title)
        formData.append('acc',this.state.acc)
        formData.append('id',this.state.id)
        formData.append('token',this.state.token)
        axios.post(ApiUrl.AdsSettingUpdate, formData)
            .then(res=>{
                if(res.data.success){
                    toast.success(res.data.success, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    setTimeout(
                        () => Router.push(`/admin/ads/settings`),
                        2000
                    )
                }else{
                    toast.error(res.data.error, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
            })
            .catch(error=>{
                console.log(error)
            })
        event.preventDefault()
    }

    render() {
        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Head>
                        <title>Ads Configure Update</title>
                    </Head>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <Head>
                        <title>Ads Configure Update </title>
                    </Head>

                    <div className="container-fluid">
                        <form onSubmit={this.FormSubmit}>
                            <div className="row  mb-5 post_create">
                                <h3 className="col-md-12 p-3 post_head mb-3">Update Ads Configure</h3>
                                <div className="col-md-6">
                                    <div className="form-group row">
                                        <label htmlFor="inputEmail3" className="col-sm-12 col-form-label"> Bank Name</label>
                                        <div className="col-sm-12">
                                            <input onChange={this.title} value={this.state.title} required type="text" className="form-control " id="inputEmail3"
                                                   placeholder="Bank name"/>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label htmlFor="inputEmail3" className="col-sm-12 col-form-label"> Account Number </label>
                                        <div className="col-sm-12">
                                            <input onChange={this.acc} value={this.state.acc} required type="text" className="form-control " id="inputEmail3"
                                                   placeholder="Account Number"/>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label htmlFor="inputEmail3" className="col-sm-12 col-form-label"> Rate </label>
                                        <div className="col-sm-12">
                                            <input onChange={this.rate} value={this.state.rate} required type="text" className="form-control " id="inputEmail3"
                                                   placeholder="Rate Per Money Views"/>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label htmlFor="inputPassword3"
                                               className="col-sm-12 col-form-label">Description </label>
                                        <div className="col-sm-12">
                                            <textarea required rows="6" value={this.state.des} onChange={this.des} className="form-control"></textarea>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <div className="col-sm-12">
                                            <button type="submit" className="btn btn-primary mt-3" style={{float:'right'}}>Update</button>
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </form>
                    </div>

                </ContentMain>
            );
        }
    }
}

export default Id;