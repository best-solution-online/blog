import React, {Component} from 'react';
import ContentMain from "../../component/content/ContentMain";
import Head from "next/head";
import {DataContext} from "../../context/ContextApi";
import Router from "next/router";
import jwtDecode from "jwt-decode";
import Link from "next/link";
import axios from "axios";
import ApiUrl from "../../component/AppApiUrl/ApiUrl";
import Loading from "../../component/loading/Loading";
import {toast} from "react-toastify";

class Category extends Component {
    static contextType = DataContext;
    constructor() {
        super();
        this.state={
           token:'',loading:true,records:[]
        }
    }

    componentDidMount() {
        const token = localStorage.getItem('token')
        if(token){
            const decoded = jwtDecode(token)
            if(decoded.role=="Admin"){
                const myFormData = new FormData()
                myFormData.append('token',token)
                axios.post(ApiUrl.CategoryAll,myFormData)
                    .then(res=>{
                        this.setState({records:res.data,token:token,loading:false})
                        console.log(res.data+"Category List Get")
                    })
                    .catch(error=>{
                        this.setState({token:token,loading:false})
                        console.log(error)
                    })
            }else{
                Router.push('/')
            }
        }else{
            Router.push('/')
        }
    }

    onDel=(id)=>{
        const token = this.state.token
        const formData = new FormData()
        formData.append('id',id)
        formData.append('token',token)
        var confirm = window.confirm("Are you sure to delete ")
        if(confirm){
            axios.post(ApiUrl.CategoryDelete,formData)
                .then(res=>{
                    this.componentDidMount()
                    if(res.data.success){
                        toast.success(res.data.success, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });

                    }else{
                        toast.error(res.data.error, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });

                    }
                })
                .catch(err=>{
                    console.log(err)
                })
        }
    }

    render() {
       if(this.state.loading==true){
           return (
               <ContentMain>
                   <Head>
                       <title>Admin Category </title>
                   </Head>
                   <div className="container-fluid">
                       <div className="row">
                           <h5 className="col-md-12 col-12 p-3 text-center cat-top">Category List</h5>
                       </div>
                   </div>
                   <Loading/>
               </ContentMain>
           )
       }else{
           return (
               <ContentMain>
                   <Head>
                       <title>Admin Category List</title>
                   </Head>

                   <div className="container-fluid">
                       <div className="row">
                           <h5 className="col-md-12 col-12 p-3 text-center cat-top">Category List ({this.state.records.length})</h5>
                           <div className="col-md-12 col-12 mt-3">
                               <Link href='/admin/category/create'>
                                   <button className="btn btn-primary mb-3">Create</button>
                               </Link>
                               {this.state.records.length>0?
                                   <table className="table table-bordered">
                                       <thead>
                                       <tr>
                                           <th scope="col">SL</th>
                                           <th scope="col">Category Name</th>
                                           <th scope="col">Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       {this.state.records.map((res,key)=>{
                                           return(
                                               <tr>
                                                   <th >{key+1}</th>
                                                   <td>{res.category}</td>
                                                   <td>
                                                       <Link href={"/admin/category/edit/"+res.id} >
                                                           <button className="btn btn-success btn-md ">
                                                               <i className="fa fa-edit"></i></button>
                                                       </Link>
                                                       <button onClick={this.onDel.bind(this,res.id)}
                                                               className="btn btn-danger btn-md">
                                                           <i className="fa fa-trash"></i>
                                                       </button>
                                                   </td>
                                               </tr>
                                           )
                                       })}
                                       </tbody>
                                   </table>
                               :
                                   <h6 className="text-center mt-5 mb-5">Not avilable</h6>
                               }

                           </div>
                       </div>
                   </div>

               </ContentMain>
           );
       }
    }
}

export default Category;