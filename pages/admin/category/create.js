import React, {Component} from 'react';
import {DataContext} from "../../../context/ContextApi";
import jwtDecode from "jwt-decode";
import axios from "axios";
import ApiUrl from "../../../component/AppApiUrl/ApiUrl";
import Router from "next/router";
import {toast} from "react-toastify";
import ContentMain from "../../../component/content/ContentMain";
import Loading from "../../../component/loading/Loading";
import Head from "next/head";
import Link from "next/link";

class Create extends Component {
    static contextType = DataContext;
    constructor() {
        super();
        this.state={
            token:'',loading:true,category:''
        }
    }

    componentDidMount() {
        const token = localStorage.getItem('token')
        if(token){
            const decoded = jwtDecode(token)
            if(decoded.role=="Admin"){
                this.setState({token:token,loading:false})
            }else{
                Router.push('/')
            }
        }else{
            Router.push('/')
        }
    }

    category=(e)=>{
        this.setState({category:e.target.value})
    }

    onForm=(e)=>{
        const formData = new FormData()
        formData.append('token',this.state.token)
        formData.append('category',this.state.category)
        if(this.state.category !=""){
            axios.post(ApiUrl.CategoryCreate,formData)
                .then(res=>{

                    if(res.data.success){
                        toast.success(res.data.success, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                        Router.push('/admin/category')

                    }else{
                        toast.error(res.data.error, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        })}

                })
                .catch(error=>{
                    console.log(error)
                })
        }
        e.preventDefault()
    }

    render() {
        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Head>
                        <title>Admin Category Create</title>
                    </Head>
                    <div className="container-fluid">
                        <div className="row">
                            <h5 className="col-md-12 col-12 p-3 text-center bg-dark text-white">Category Create</h5>
                        </div>
                    </div>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <Head>
                        <title>Admin Category Create</title>
                    </Head>

                    <div className="container-fluid">
                        <div className="row">
                            <h5 className="col-md-12 col-12 p-3 text-center cat-top">Category Create</h5>
                            <div className="col-md-2"></div>
                            <div className="col-md-8 col-12 mt-5 cat">
                                <form onSubmit={this.onForm.bind(this)}>
                                    <div className="form-group row">
                                        <label htmlFor="colFormLabelLg"
                                               className="col-sm-4 col-form-label col-form-label-lg">Category name</label>
                                        <div className="col-sm-8">
                                            <input type="text" className="form-control form-control-lg" onChange={this.category.bind(this)}
                                                   value={this.state.category}
                                                   id="colFormLabelLg" placeholder="Category name"/>
                                        </div>
                                    </div>
                                    <div className="form-group row ">
                                        <div className="col-sm-12">
                                            <button type="submit" className="btn btn-primary float-end mt-3">Submit</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                </ContentMain>
            );
        }
    }
}

export default Create;