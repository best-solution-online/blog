import React, {Component} from 'react';
import jwtDecode from "jwt-decode";
import Router from "next/router";
import axios from "axios";
import ApiUrl from "../../../../component/AppApiUrl/ApiUrl";
import {toast} from "react-toastify";
import ContentMain from "../../../../component/content/ContentMain";
import Loading from "../../../../component/loading/Loading";
import Head from "next/head";

export async function getServerSideProps({query}) {
    const s = "Ok"
    return {
        props: {query}
    }

}

class Cid extends Component {
    constructor() {
        super();
        this.state={
            token:'',loading:true,category:'',id:''
        }
    }

    componentDidMount() {
        const token = localStorage.getItem('token')
        if(token){
            const decoded = jwtDecode(token)
            if(decoded.role=="Admin"){
               console.log(this.props.query.cid)
                const formData = new FormData()
                formData.append('token',this.state.token)
                formData.append('id',this.props.query.cid)
                axios.post(ApiUrl.CategoryOne,formData)
                    .then(res=>{
                        this.setState({id:res.data[0]['id'],category:res.data[0]['category'],token:token,loading:false})
                    })
                    .catch(error=>{
                        console.log(error)
                    })

            }else{
                Router.push('/')
            }
        }else{
            Router.push('/')
        }
    }

    category=(e)=>{
        this.setState({category:e.target.value})
    }

    onForm=(e)=>{
        const formData = new FormData()
        formData.append('token',this.state.token)
        formData.append('id',this.state.id)
        formData.append('category',this.state.category)
        axios.post(ApiUrl.CategoryUpdate,formData)
            .then(res=>{
                if(res.data.success){
                    toast.success(res.data.success, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    Router.push('/admin/category')

                }else{
                    toast.error(res.data.error, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    })}

            })
            .catch(error=>{
                console.log(error)
            })
        e.preventDefault()
    }

    render() {
        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Head>
                        <title>Admin Category Edit</title>
                    </Head>
                    <div className="container-fluid">
                        <div className="row">
                            <h5 className="col-md-12 col-12 p-3 text-center bg-dark text-white">Category Update</h5>
                        </div>
                    </div>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <Head>
                        <title>Admin Category Edit</title>
                    </Head>

                    <div className="container-fluid">
                        <div className="row">
                            <h5 className="col-md-12 col-12 p-3 text-center  cat-top">Category Update</h5>
                            <div className="col-md-2"></div>
                            <div className="col-md-8 col-12 mt-5 cat">
                                <form onSubmit={this.onForm.bind(this)}>
                                    <div className="form-group row">
                                        <label htmlFor="colFormLabelLg"
                                               className="col-sm-4 col-form-label col-form-label-lg">Category name</label>
                                        <div className="col-sm-8">
                                            <input type="text" className="form-control form-control-lg" onChange={this.category.bind(this)}
                                                   value={this.state.category}
                                                   id="colFormLabelLg" />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <div className="col-sm-12">
                                            <button type="submit" className="btn btn-primary float-end mt-3">Submit</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                </ContentMain>
            );
        }
    }
}

export default Cid;