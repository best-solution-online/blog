import React, {Component} from 'react';
import ContentMain from "../../component/content/ContentMain";
import Head from "next/head";
import Link from 'next/link'
import {DataContext} from "../../context/ContextApi";
import jwtDecode from "jwt-decode";
import axios from "axios";
import ApiUrl from "../../component/AppApiUrl/ApiUrl";
import Router from "next/router";
import Loading from "../../component/loading/Loading";
class Index extends Component {
    static contextType = DataContext;
    constructor() {
        super();
        this.state={
            token:'',loading:true,records:[]
        }
    }

    componentDidMount() {
        const token = localStorage.getItem('token')
        if(token){
            const decoded = jwtDecode(token)
            if(decoded.role=="Admin"){
                this.setState({token:token,loading:false})
            }else{
                Router.push('/')
            }
        }else{
            Router.push('/')
        }
    }
    render() {
        const {ads,contact,privacy,about,terms,icon,logo,fb,youtube,twitter,instagram,linkedin,footer,title,des,tags,meta}=this.context;
        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <Head>
                        <title>Admin Dashboard</title>
                    </Head>
                    <div className="dashboard">
                        <h3 className="col-md-12 col-12 p-3 post_head mb-3">Admin Dashboard</h3>
                        <div className="py-3">
                            <div className="container">
                                <div className="row " >
                                    <div className="col-lg-4 col-12 mb-3">
                                        <div className="card text-center">
                                            <div className="card-body card-body-home text-center">
                                                <p className=" text-center pt-1"><i className="fa fa-users"></i> Total User </p>
                                                <h3 className="card-text pt-1"> </h3>
                                                <Link href="/admin/user" className="btn btn-dark col-md-12 col-12 homebtn"> Manage User </Link>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 col-12 mb-3">
                                        <div className="card text-center">
                                            <div className="card-body card-body-home text-center">
                                                <p className=" text-center pt-1"><i className="fa fa-pen"></i> Total Post </p>
                                                <h3 className="card-text pt-1"> </h3>
                                                <Link href="/admin/post" className="btn btn-dark col-md-12 col-12 homebtn"> Manage Post </Link>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-lg-4 col-12 mb-3">
                                        <div className="card text-center">
                                            <div className="card-body card-body-home text-center">
                                                <p className=" text-center pt-1"><i className="fa fa-pen"></i> Total Category </p>
                                                <h3 className="card-text pt-1"> </h3>
                                                <Link href="/admin/category" className="btn btn-dark col-md-12 col-12 homebtn"> Manage Category </Link>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-lg-4 col-12 mb-3">
                                        <div className="card text-center">
                                            <div className="card-body card-body-home text-center">
                                                <p className=" text-center pt-1"><i className="fa fa-location-arrow"></i>Total Ads  </p>
                                                <h3 className="card-text pt-1"> </h3>
                                                <Link href="/admin/ads" className="btn btn-dark col-md-12 col-12 homebtn"> Manage Ads  </Link>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-lg-4 col-12 mb-3">
                                        <div className="card text-center">
                                            <div className="card-body card-body-home text-center">
                                                <p className=" text-center pt-1"><i className="fa fa-location-arrow"></i> Ads Settings </p>
                                                <h3 className="card-text pt-1"> </h3>
                                                <Link href="/admin/ads/settings" className="btn btn-dark col-md-12 col-12 homebtn"> Manage Ads Settings </Link>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="col-lg-4 col-12 mb-3">
                                        <div className="card text-center">
                                            <div className="card-body card-body-home text-center">
                                                <p className=" text-center pt-1"><i className="fa fa-book"></i> Total Report </p>
                                                <h3 className="card-text pt-1"> </h3>
                                                <Link href="/admin/report" className="btn btn-dark col-md-12 col-12 homebtn"> Manage Report</Link>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <h3 className="col-md-12 p-3 post_head mb-3">Settings  </h3>
                        <div className="container">
                            <Link href='/admin/settings_edit'><a className="btn btn-dark ml-1 col-md-2 ">Settings Edit</a></Link>
                            <div className="row m-2 mt-3 bg-white">
                                <div className="col-md-2 p-md-3 p-col-3  ">
                                    <h4>Logo</h4>
                                    <img style={{height:'50px',width:'100px'}} src={ApiUrl.photoUrl+logo}/>
                                </div>
                                <div className="col-md-2 p-md-3 p-col-3  ">
                                    <h4>Favicon Icon</h4>
                                    <img style={{height:'50px',width:'100px'}} src={ApiUrl.photoUrl+icon}/>
                                </div>
                                <div className="col-md-4 p-md-3 p-col-3  ">
                                    <h4>Site Title</h4>
                                    <p>{title}</p>
                                </div>
                                <div className="col-md-4 p-md-3 p-col-3  ">
                                    <h4>Site Footer </h4>
                                    <p>{footer}</p>
                                </div>

                            </div>
                        </div>
                        <div className="container">
                            <div className="row m-2 mt-3">
                                <div className="col-md-12 p-md-5 p-col-3 bg-white ">
                                    <h3 className="text-center mb-3 mt-2">Website Description </h3>
                                    {des}
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="row m-2 mt-3">
                                <div className="col-md-12 p-md-5 p-col-3 bg-white ">
                                    <h3 className="text-center mb-3 mt-2">Website Keyword</h3>
                                    {tags}
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="row m-2 mt-3">
                                <div className="col-md-12 p-md-5 p-col-3 bg-white ">
                                    <h3 className="text-center mb-3 mt-2">Advertising</h3>
                                    {ads}
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="row m-2 mt-3">
                                <div className="col-md-12 p-md-5 p-col-3 bg-white ">
                                    <h3 className="text-center mb-3 mt-2">Privacy Policy</h3>
                                    {privacy}
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="row m-2 mt-3">
                                <div className="col-md-12 p-md-5 p-col-3 bg-white ">
                                    <h3 className="text-center mb-3 mt-2">Terms & Condition</h3>
                                    {terms}
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="row m-2 mt-3">
                                <div className="col-md-12 p-md-5 p-col-3 bg-white ">
                                    <h3 className="text-center mb-3 mt-2">About US</h3>
                                    {about}
                                </div>
                            </div>
                        </div>
                        <div className="container">
                            <div className="row m-2 mt-3">
                                <div className="col-md-12 p-md-5 p-col-3 bg-white ">
                                    <h3 className="text-center mb-3 mt-2">Contact US</h3>
                                    {contact}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12 p-md-3 p-col-3  ">
                            <h4>Facebook</h4>
                            <p>{fb}</p>
                            <h4>Youtube</h4>
                            <p>{youtube}</p>
                            <h4>Twitter</h4>
                            <p>{twitter}</p>
                            <h4>Instagram</h4>
                            <p>{instagram}</p>
                            <h4>Linkedin</h4>
                            <p>{linkedin}</p>
                        </div>
                    </div>
                </ContentMain>
            );
        }
    }
}

export default Index;