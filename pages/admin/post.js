import React, {Component} from 'react';
import axios from "axios";
import ApiUrl from "../../component/AppApiUrl/ApiUrl";
import Router from "next/router";
import {toast} from "react-toastify";
import moment from "moment";
import Link from "next/link";
import ContentMain from "../../component/content/ContentMain";
import Head from "next/head";
import jwtDecode from "jwt-decode";
import Loading from "../../component/loading/Loading";

class Post extends Component {
    constructor() {
        super();
        this.state={
            token:'',post:[],name:'',spost:[],loading:true
        }
    }
    componentDidMount() {
        const token = localStorage.token
        if(token){
            this.setState({token:token})
            const decoded = jwtDecode(token)
            if(decoded.role=="Admin"){
                const FormDa = new FormData()
                FormDa.append('token',token)
                axios.post(ApiUrl.PostTrend,FormDa)
                    .then(res=>{
                        console.log(res)
                        this.setState({post:res.data,loading:false})
                    })
                    .catch(error=>{
                        this.setState({loading:false})
                        console.log(error)
                    })
            }

        }else{
            Router.push('/')
        }
    }

    name=(e)=>{
        this.setState({name:e.target.value})
    }

    submit=(event)=>{
        const FormDa = new FormData()
        FormDa.append('token',this.state.token)
        FormDa.append('name',this.state.name)
        axios.post(ApiUrl.PostSearch,FormDa)
            .then(res=>{
                console.log(res)
                this.setState({spost:res.data})
            })
            .catch(error=>{
                console.log(error)
            })
        event.preventDefault()
    }


    onDel=(id)=>{
        var confirm = window.confirm("Are you sure to delete ")
        const FormDa = new FormData()
        FormDa.append('token',this.state.token)
        FormDa.append('id',id)

        if(confirm){
            axios.post(ApiUrl.PostDelete,FormDa)
                .then(res=>{
                    if(res.data.success){
                        this.componentDidMount()
                        toast.success(res.data.success, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });

                    }else{
                        toast.error(res.data.error, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });

                    }
                })
                .catch(error=>{
                    //localStorage.removeItem('token')
                    console.log(error)
                })
        }
    }

    render() {
        const data = this.state.post.map((res,key)=>{
            return(
                <tr>
                    <th >{key+1}</th>
                    <th ><Link href={"/channel/"+res.u_slug}>{res.u_name}</Link></th>
                    <td> {res.img ? <Link href={"/watch?p="+res.slug}><a>
                            <img src={ApiUrl.photoUrl+res.img} style={{height:'60px',width:'100px'}} />
                        </a></Link> :
                        <Link href={"/watch?p="+res.slug}><a>
                            <img src="/noimage.png" style={{height:'60px',width:'100px'}} />
                        </a></Link>
                    } </td>
                    <td style={{width:'200px'}}>
                        <Link href={"/watch?p="+res.slug}><a>{res.title.substring(0,70)}...</a></Link>
                    </td>
                    <td style={{width:'300px'}}>
                        <Link href={"/watch?p="+res.slug}><a>{res.body.substring(0,100)}...</a></Link>
                    </td>
                    <th >{res.views}</th>
                    <th >{res.status}</th>
                    <td>{moment(res.date).format("MMM Do YY")}</td>
                    <td style={{width:'120px'}}>
                        <Link href={"/post/edit/"+res.id}>
                            <button className="btn btn-success btn-sm">
                                Edit
                            </button>
                        </Link>
                        <button onClick={this.onDel.bind(this,res.id)} className="btn btn-danger btn-sm">
                            Delete
                        </button>
                    </td>
                </tr>
            )
        })

        const sdata = this.state.spost.map((res,key)=>{
            return(
                <tr>
                    <th >{key+1}</th>
                    <th ><Link href={"/channel/"+res.u_slug}>{res.u_name}</Link></th>
                    <td> {res.img ? <Link href={"/watch?p="+res.slug}><a>
                            <img src={ApiUrl.photoUrl+res.img} style={{height:'60px',width:'100px'}} />
                        </a></Link> :
                        <Link href={"/watch?p="+res.slug}><a>
                            <img src="/noimage.png" style={{height:'60px',width:'100px'}} />
                        </a></Link>
                    } </td>
                    <td style={{width:'200px'}}>
                        <Link href={"/watch?p="+res.slug}><a>{res.title.substring(0,70)}...</a></Link>
                    </td>
                    <td style={{width:'300px'}}>
                        <Link href={"/watch?p="+res.slug}><a>{res.body.substring(0,100)}...</a></Link>
                    </td>
                    <th >{res.views}</th>
                    <th >{res.status}</th>
                    <td>{moment(res.date).format("MMM Do YY")}</td>
                    <td style={{width:'120px'}}>
                        <Link href={"/post/edit/"+res.id}>
                            <button className="btn btn-success btn-sm">
                                Edit
                            </button>
                        </Link>
                        <button onClick={this.onDel.bind(this,res.id)} className="btn btn-danger btn-sm">
                            Delete
                        </button>
                    </td>
                </tr>
            )
        })

        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Head>
                        <title>All Post </title>
                    </Head>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <Head>
                        <title>All Post </title>
                    </Head>
                    <div className="container-fluid bg-white">
                        <div className="row mb-5 post_create">
                            <h3 className="col-md-12 p-3 post_head mb-3">All content
                                ({this.state.spost.length>0?this.state.spost.length:this.state.post.length})</h3>

                            {
                                this.state.post.length>0?
                                    <>
                                    <div className="col-md-6 col-lg-6 col-8 search">
                                        <div className="input-group mb-3 ssection">
                                            <input  type="text" className="form-control" placeholder=" Filter by type" style={{height:'30px'}}
                                                    onChange={this.name} value={this.state.name} aria-label="Recipient's username" aria-describedby="basic-addon2"/>
                                            <div className="input-group-append">
                                                <button onClick={this.submit} style={{height:'30px'}}
                                                        className="btn btn-light searchbtn" title="Search" type="button"><i className="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="table-responsive">
                                        <table className="table " style={{width:'100%'}}>
                                            <tbody>
                                            <tr>
                                                <th scope="col">Sl</th>
                                                <th scope="col">User</th>
                                                <th scope="col">Image</th>
                                                <th scope="col">Title</th>
                                                <th scope="col">Description</th>
                                                <th scope="col">Views</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">Date</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                            {this.state.spost.length>0?sdata:data}
                                            </tbody>
                                        </table>
                                    </div>
                                    </>
                                :
                                    <h6 className="text-center mt-5 mb-5">Not avilable</h6>
                            }

                        </div>
                    </div>
                </ContentMain>
            );
        }


    }
}

export default Post;