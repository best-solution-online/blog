import React, {Component} from 'react';
import jwtDecode from "jwt-decode";
import axios from "axios";
import ApiUrl from "../../component/AppApiUrl/ApiUrl";
import Router from "next/router";
import {toast} from "react-toastify";
import Link from "next/link";
import moment from "moment";
import ContentMain from "../../component/content/ContentMain";
import Head from "next/head";
import Loading from "../../component/loading/Loading";

class Report extends Component {
    constructor() {
        super();
        this.state={
            token:'',post:[],loading:true
        }
    }
    componentDidMount() {
        const token = localStorage.token
        if(token){
            this.setState({token:token})
            const decoded = jwtDecode(token)
            if(decoded.role=="Admin"){
                const FormDa = new FormData()
                FormDa.append('token',token)
                axios.post(ApiUrl.ReportAll,FormDa)
                    .then(res=>{
                        console.log(res)
                        this.setState({post:res.data,loading:false})
                    })
                    .catch(error=>{
                        this.setState({loading:false})
                        console.log(error)
                    })
            }else{
                Router.push('/')
            }

        }else{
            Router.push('/')
        }
    }





    onDel=(id)=>{
        var confirm = window.confirm("Are you sure to delete ")
        const FormDa = new FormData()
        FormDa.append('token',this.state.token)
        FormDa.append('id',id)
        if(confirm){
            axios.post(ApiUrl.ReportDelete,FormDa)
                .then(res=>{
                    if(res.data.success){
                        this.componentDidMount()
                        toast.success(res.data.success, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });

                    }else{
                        toast.error(res.data.error, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });

                    }
                })
                .catch(error=>{
                    console.log(error)
                })
        }
    }
    render() {
        const data = this.state.post.map((res,key)=>{
            return(
                <tr>
                    <th >{key+1}</th>
                    <th ><Link href={"/channel/"+res.u_slug}>{res.u_name}</Link></th>
                    <td style={{width:'600px'}}>{res.report}</td>
                    <td style={{width:'70px'}}>{moment(res.date).format("MMM Do YY")}</td>
                    <td> {res.img ? <Link href={"/watch?p="+res.slug}><a>
                            <img src={ApiUrl.photoUrl+res.img} style={{height:'60px',width:'100px'}} />
                        </a></Link> :
                        <Link href={"/watch?p="+res.slug}><a>
                            <img src="/noimage.png" style={{height:'60px',width:'100px'}} />
                        </a></Link>
                    } </td>
                    <td style={{width:'200px'}}>
                        <Link href={"/watch?p="+res.slug}><a>{res.title.substring(0,70)}...</a></Link>
                    </td>
                    <th >{res.views}</th>
                    <td style={{width:'50px'}}>
                        <button onClick={this.onDel.bind(this,res.report_id)} className="btn btn-danger btn-sm">
                            Delete
                        </button>
                    </td>
                </tr>
            )
        })


        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Head>
                        <title>All Post Report </title>
                    </Head>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <Head>
                        <title>All Post Report </title>
                    </Head>
                    <div className="container-fluid bg-white">
                        <div className="row mb-5 post_create">
                            <h3 className="col-md-12 p-3 post_head mb-3">All content report ({this.state.post.length})</h3>

                            {this.state.post.length>0 ?
                                <div className="table-responsive">
                                    <table className="table " style={{width:'100%'}}>
                                        <tbody>
                                        <tr>
                                            <th scope="col">Sl</th>
                                            <th scope="col">Reporter name</th>
                                            <th scope="col">Report text</th>
                                            <th scope="col">Report date</th>
                                            <th scope="col">Image</th>
                                            <th scope="col">Title</th>
                                            <th scope="col">Views</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        {data}
                                        </tbody>
                                    </table>
                                </div>
                            :
                                <h6 className="text-center mt-5 mb-5">Not avilable</h6>
                            }


                        </div>
                    </div>
                </ContentMain>
            );
        }


    }
}

export default Report;