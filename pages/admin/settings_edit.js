import React, {Component} from 'react';
import ContentMain from "../../component/content/ContentMain";
import jwtDecode from "jwt-decode";
import Router from "next/router";
import axios from "axios";
import ApiUrl from "../../component/AppApiUrl/ApiUrl";
import Loading from "../../component/loading/Loading";
import Head from 'next/head'
import {toast} from "react-toastify";
class SettingsEdit extends Component {
    constructor() {
        super();
        this.state={
            logo:'',icon:'',title:'',footer:'',tags:'',meta:'',des:'',fb:'',youtube:'',twitter:'',logo1:'',icon1:'',
            instagram:'',linkedin:'',about:'',contact:'',terms:'',privacy:'',ads:'',loading:true
        }
    }
    componentDidMount() {
        const token = localStorage.getItem('token')
        if(token){
            const decoded = jwtDecode(token)
            if(decoded.role=="Admin"){
                axios.post(ApiUrl.SettingsOne)
                    .then(res=>{
                        this.setState({logo:res.data[0]['logo'],icon:res.data[0]['icon'],
                            title:res.data[0]['title'],footer:res.data[0]['footer'],tags:res.data[0]['tags'],
                            meta:res.data[0]['meta'],des:res.data[0]['des'],fb:res.data[0]['fb'],
                            youtube:res.data[0]['youtube'],twitter:res.data[0]['twitter'],instagram:res.data[0]['instagram'],
                            linkedin:res.data[0]['linkedin'],about:res.data[0]['about'],contact:res.data[0]['contact'],
                            terms:res.data[0]['terms'],privacy:res.data[0]['privacy'],ads:res.data[0]['ads'],token:token,loading:false
                        })
                    })
                    .catch(error=>{
                        console.log(error)
                    })
            }else{
                Router.push('/')
            }
        }else{
            Router.push('/')
        }
    }

    title=(e)=>{
        this.setState({title:e.target.value})
    }
    footer=(e)=>{
        this.setState({footer:e.target.value})
    }
    des=(e)=>{
        this.setState({des:e.target.value})
    }
    tags=(e)=>{
        this.setState({tags:e.target.value})
    }
    about=(e)=>{
        this.setState({about:e.target.value})
    }
    contact=(e)=>{
        this.setState({contact:e.target.value})
    }
    ads=(e)=>{
        this.setState({ads:e.target.value})
    }
    privacy=(e)=>{
        this.setState({privacy:e.target.value})
    }
    terms=(e)=>{
        this.setState({terms:e.target.value})
    }
    fb=(e)=>{
        this.setState({fb:e.target.value})
    }
    youtube=(e)=>{
        this.setState({youtube:e.target.value})
    }
    twitter=(e)=>{
        this.setState({twitter:e.target.value})
    }
    instagram=(e)=>{
        this.setState({instagram:e.target.value})
    }
    linkedin=(e)=>{
        this.setState({linkedin:e.target.value})
    }
    logo=(e)=>{
        this.setState({logo:e.target.files[0]})
        var file = e.target.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function (e) {
            this.setState({
                logo1: [reader.result]
            })
        }.bind(this);
    }
    icon=(e)=>{
        this.setState({icon:e.target.files[0]})
        var file = e.target.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function (e) {
            this.setState({
                icon1: [reader.result]
            })
        }.bind(this);
    }

    FormSubmit=(event)=>{
        const formData = new FormData()
        formData.append('title',this.state.title)
        formData.append('des',this.state.des)
        formData.append('footer',this.state.footer)
        formData.append('tags',this.state.tags)
        formData.append('about',this.state.about)
        formData.append('contact',this.state.contact)
        formData.append('logo',this.state.logo)
        formData.append('icon',this.state.icon)
        formData.append('terms',this.state.terms)
        formData.append('privacy',this.state.privacy)
        formData.append('ads',this.state.ads)
        formData.append('fb',this.state.fb)
        formData.append('youtube',this.state.youtube)
        formData.append('twitter',this.state.twitter)
        formData.append('instagram',this.state.instagram)
        formData.append('linkedin',this.state.linkedin)
        formData.append('token',this.state.token)
        axios.post(ApiUrl.SettingsUpdate, formData)
            .then(res=>{
                if(res.data.success){
                    this.componentDidMount()
                    toast.success(res.data.success, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    //Router.push(`/admin`)
                    setTimeout(
                        () => Router.push(`/admin`),
                        3000
                    )
                }else{
                    toast.error(res.data.error, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
            })
            .catch(error=>{
                console.log(error)
            })
        event.preventDefault()
    }


    render() {
     if(this.state.loading==true){
         return(
             <ContentMain>
                 <Loading/>
             </ContentMain>
         )
     }else{
         return (
             <ContentMain>
                 <Head>
                     <title>Site Settings Update</title>
                 </Head>
                 <div className="container-fluid">
                     <form onSubmit={this.FormSubmit} >
                         <div className="row  mb-5 post_create">
                             <h3 className="col-md-12 p-3 post_head mb-3">Update Settings</h3>
                             <div className="col-md-6">
                                 <div className="form-group row">
                                     <label htmlFor="inputEmail3" className="col-sm-12 col-form-label">Title (required)</label>
                                     <div className="col-sm-12">
                                         <input  value={this.state.title} onChange={this.title} required type="text" className="form-control " id="inputEmail3"
                                                placeholder="Title Here"/>
                                     </div>
                                 </div>
                                 <div className="form-group row">
                                     <label htmlFor="inputEmail3" className="col-sm-12 col-form-label"> Footer (required)</label>
                                     <div className="col-sm-12">
                                         <input  value={this.state.footer} onChange={this.footer} required type="text" className="form-control " id="inputEmail3"
                                                 placeholder="Footer Here"/>
                                     </div>
                                 </div>
                                 <div className="form-group row">
                                     <label htmlFor="inputPassword3"
                                            className="col-sm-12 col-form-label">Description </label>
                                     <div className="col-sm-12">
                                         <textarea required rows="8"onChange={this.des} value={this.state.des}  className="form-control"></textarea>
                                     </div>
                                 </div>
                                 <div className="form-group row">
                                     <label htmlFor="inputPassword3"
                                            className="col-sm-12 col-form-label">SEO Tags</label>
                                     <div className="col-sm-12">
                                         <textarea rows="3" className="form-control" onChange={this.tags} value={this.state.tags}  placeholder="tag, tag, tag, tag"></textarea>
                                     </div>
                                 </div>
                                 <div className="form-group row">
                                     <label htmlFor="inputPassword3"
                                            className="col-sm-12 col-form-label">About</label>
                                     <div className="col-sm-12">
                                         <textarea rows="3" className="form-control" onChange={this.about} value={this.state.about} ></textarea>
                                     </div>
                                 </div>
                                 <div className="form-group row">
                                     <label htmlFor="inputPassword3"
                                            className="col-sm-12 col-form-label">Contact</label>
                                     <div className="col-sm-12">
                                         <textarea rows="3" className="form-control" onChange={this.contact} value={this.state.contact} ></textarea>
                                     </div>
                                 </div>
                                 <div className="form-group row">
                                     <label htmlFor="inputPassword3"
                                            className="col-sm-12 col-form-label">Terms</label>
                                     <div className="col-sm-12">
                                         <textarea rows="3" className="form-control" onChange={this.terms} value={this.state.terms} ></textarea>
                                     </div>
                                 </div>

                             </div>
                             <div className="col-md-6">
                                 <div className="form-group row">
                                     <label htmlFor="inputPassword3"
                                            className="col-sm-12 col-form-label">Privacy Policy</label>
                                     <div className="col-sm-12">
                                         <textarea rows="3" className="form-control" onChange={this.privacy} value={this.state.privacy} ></textarea>
                                     </div>
                                 </div>
                                 <div className="form-group row">
                                     <label htmlFor="inputPassword3"
                                            className="col-sm-12 col-form-label">Ads</label>
                                     <div className="col-sm-12">
                                         <textarea rows="3" className="form-control" onChange={this.ads} value={this.state.ads} ></textarea>
                                     </div>
                                 </div>
                                 <div className="form-group row">
                                     <label htmlFor="inputPassword3"
                                            className="col-sm-12 col-form-label">Logo Image</label>
                                     <div className="col-sm-12">
                                         <input className="form-control" onChange={this.logo} type="file" />
                                     </div>
                                     <div className="col-sm-12">
                                         {this.state.logo1?<img id="showImage" className="mt-3 mb-3" style={{height:'40px'}} src={this.state.logo1} />:
                                             this.state.logo ? <img className="mt-3 mb-3" style={{height:'40px'}} src={ApiUrl.photoUrl+this.state.logo}/> :''
                                         }
                                     </div>
                                 </div>
                                 <div className="form-group row">
                                     <label htmlFor="inputPassword3"
                                            className="col-sm-12 col-form-label">Favicon Image</label>
                                     <div className="col-sm-12">
                                         <input className="form-control" onChange={this.icon} type="file" />
                                     </div>
                                     <div className="col-sm-12">
                                         {this.state.icon1?<img id="showImage" className="mt-3 mb-3" style={{height:'40px'}} src={this.state.icon1} />:
                                             this.state.icon ? <img className="mt-3 mb-3" style={{height:'40px'}} src={ApiUrl.photoUrl+this.state.icon}/> :''
                                         }
                                     </div>
                                 </div>
                                 <div className="form-group row">
                                     <label htmlFor="inputEmail3" className="col-sm-12 col-form-label">Facebook</label>
                                     <div className="col-sm-12">
                                         <input  value={this.state.fb} onChange={this.fb} type="text" className="form-control " id="inputEmail3"
                                                 placeholder="Facebook Here"/>
                                     </div>
                                 </div>
                                 <div className="form-group row">
                                     <label htmlFor="inputEmail3" className="col-sm-12 col-form-label">Youtube</label>
                                     <div className="col-sm-12">
                                         <input  value={this.state.youtube} onChange={this.youtube} type="text" className="form-control " id="inputEmail3"
                                                 placeholder="Youtube Here"/>
                                     </div>
                                 </div>
                                 <div className="form-group row">
                                     <label htmlFor="inputEmail3" className="col-sm-12 col-form-label">Twitter</label>
                                     <div className="col-sm-12">
                                         <input  value={this.state.twitter} onChange={this.twitter} type="text" className="form-control " id="inputEmail3"
                                                 placeholder="Twitter Here"/>
                                     </div>
                                 </div>
                                 <div className="form-group row">
                                     <label htmlFor="inputEmail3" className="col-sm-12 col-form-label">Instagram</label>
                                     <div className="col-sm-12">
                                         <input  value={this.state.instagram} onChange={this.instagram} type="text" className="form-control " id="inputEmail3"
                                                 placeholder="Instagram Here"/>
                                     </div>
                                 </div>
                                 <div className="form-group row">
                                     <label htmlFor="inputEmail3" className="col-sm-12 col-form-label">Linkedin</label>
                                     <div className="col-sm-12">
                                         <input  value={this.state.linkedin} onChange={this.linkedin} type="text" className="form-control " id="inputEmail3"
                                                 placeholder="Linkedin Here"/>
                                     </div>
                                 </div>
                                 <div className="form-group row">
                                     <div className="col-sm-12">
                                         <button type="submit" className="btn btn-primary mt-3" style={{float:'right'}}>Update</button>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </form>
                 </div>
             </ContentMain>
         );
     }
    }
}

export default SettingsEdit;