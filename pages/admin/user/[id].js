import React, {Component} from 'react';
import jwtDecode from "jwt-decode";
import axios from "axios";
import ApiUrl from "../../../component/AppApiUrl/ApiUrl";
import Router from "next/router";
import ContentMain from "../../../component/content/ContentMain";
import Head from "next/head";
import {toast} from "react-toastify";
import Loading from "../../../component/loading/Loading";

export async function getServerSideProps({query}) {
    const s = "Ok"
    return {
        props: {query}
    }

}

class Id extends Component{
    constructor() {
        super();
        this.state={
            token:'',role:'',id:'',name:'',status:'',loading:true,
        }
    }
    componentDidMount() {
        const token = localStorage.token
        if(token){
            const decoded = jwtDecode(token)
            this.setState({token:token,role:decoded.role})
            if(decoded.role=="Admin"){
                const FormDa = new FormData()
                FormDa.append('uid',this.props.query.id)
                FormDa.append('token',token)
                axios.post(ApiUrl.UserInfo,FormDa)
                    .then(res=>{
                        console.log(res)
                        this.setState({name:res.data[0]['name'],status:res.data[0]['status'],
                            role:res.data[0]['role'],id:res.data[0]['id'],loading:false})
                    })
                    .catch(error=>{
                        console.log(error)
                    })
            }

        }else{
            Router.push('/')
        }
    }

    onstatus=(e)=>{
        this.setState({status:e.target.value})
    }

    onrole=(e)=>{
        this.setState({role:e.target.value})
    }
     data = [{id:"0",name:"Inactive"},{id:"1",name:"Active"}]
    datarole = [{id:"User",name:"User"},{id:"Admin",name:"Admin"}]

    submit=(event)=>{
        const formData = new FormData()
        formData.append('status',this.state.status)
        formData.append('role',this.state.role)
        formData.append('id',this.state.id)
        formData.append('token',this.state.token)
        axios.post(ApiUrl.ProfileStatusUpdateByAdmin, formData)
            .then(res=>{
                if(res.data.success){
                    toast.success(res.data.success, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    setTimeout(
                        () => {
                            Router.push(`/admin/user`)
                        },
                        1000
                    )
                }else{
                    toast.error(res.data.error, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
            })
            .catch(error=>{
                console.log(error)
            })
        event.preventDefault()
    }

    render() {
          if(this.state.loading==true){
              return (
                  <ContentMain>
                      <Loading/>
                  </ContentMain>
              )
          }else{
              return (
                  <ContentMain>
                      <Head>
                          <title>{this.state.name}</title>
                      </Head>
                      <div className="container-fluid">
                          <form onSubmit={this.submit}>
                              <div className="row  mb-5 post_create">
                                  <h3 className="col-md-12 p-3 post_head mb-3">Update User</h3>
                                  <div className="col-md-6">
                                      <div className="form-group row">
                                          <label htmlFor="inputEmail3" className="col-sm-12 col-form-label">Name </label>
                                          <div className="col-sm-12">
                                              <input  required type="text" className="form-control " id="inputEmail3"
                                                      value={this.state.name} readOnly />
                                          </div>
                                      </div>

                                      <div className="form-group row">
                                          <label htmlFor="inputPassword3"
                                                 className="col-sm-12 col-form-label">{this.state.name} Visibility</label>
                                          <div className="col-sm-12">
                                              <select id="inputState" onChange={this.onstatus} className="form-control">
                                                  {this.data.map(res=>{
                                                      return(
                                                          <option  value={res.id} selected={this.state.status==res.id} >{res.name}</option>
                                                      )
                                                  })}

                                              </select>
                                          </div>
                                      </div>

                                      <div className="form-group row">
                                          <label htmlFor="inputPassword3"
                                                 className="col-sm-12 col-form-label">User Role</label>
                                          <div className="col-sm-12">
                                              <select id="inputState" onChange={this.onrole} className="form-control">
                                                  {this.datarole.map(res=>{
                                                      return(
                                                          <option  value={res.id} selected={this.state.role==res.id} >{res.name}</option>
                                                      )
                                                  })}

                                              </select>
                                          </div>
                                      </div>

                                      <div className="form-group row">
                                          <div className="col-sm-12">
                                              <button type="submit" className="btn btn-primary mt-3" style={{float:'right'}}>Update</button>
                                          </div>
                                      </div>

                                  </div>

                              </div>
                          </form>
                      </div>
                  </ContentMain>
              );
          }
    }
}

export default Id;