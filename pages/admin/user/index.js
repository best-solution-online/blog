import React, {Component} from 'react';
import jwtDecode from "jwt-decode";
import axios from "axios";
import ApiUrl from "../../../component/AppApiUrl/ApiUrl";
import Router from "next/router";
import {toast} from "react-toastify";
import Link from "next/link";
import moment from "moment";
import ContentMain from "../../../component/content/ContentMain";
import Head from "next/head";
import Loading from "../../../component/loading/Loading";

class Index extends Component {
    constructor() {
        super();
        this.state={
            token:'',user:[],name:'',suser:[],loading:true
        }
    }
    componentDidMount() {
        const token = localStorage.token
        if(token){
            this.setState({token:token})
            const decoded = jwtDecode(token)
            if(decoded.role=="Admin"){
                const FormDa = new FormData()
                FormDa.append('token',token)
                axios.post(ApiUrl.UserAll,FormDa)
                    .then(res=>{
                        console.log(res)
                        this.setState({user:res.data,loading:false})
                    })
                    .catch(error=>{
                        this.setState({loading:false})
                        console.log(error)
                    })
            }

        }else{
            Router.push('/')
        }
    }

    name=(e)=>{
        this.setState({name:e.target.value})
    }

    submit=(event)=>{
        const FormDa = new FormData()
        FormDa.append('token',this.state.token)
        FormDa.append('name',this.state.name)
        axios.post(ApiUrl.UserAllSearch,FormDa)
            .then(res=>{
                console.log(res)
                this.setState({suser:res.data})
            })
            .catch(error=>{
                console.log(error)
            })
        event.preventDefault()
    }


    onDel=(id)=>{
        var confirm = window.confirm("Are you sure to delete ")
        const FormDa = new FormData()
        FormDa.append('token',this.state.token)
        FormDa.append('id',id)

        if(confirm){
            axios.post(ApiUrl.UserDelete,FormDa)
                .then(res=>{
                    if(res.data.success){
                        this.componentDidMount()
                        toast.success(res.data.success, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });

                    }else{
                        toast.error(res.data.error, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });

                    }
                })
                .catch(error=>{
                    console.log(error)
                })
        }
    }
    render() {
        const data = this.state.user.map((res,key)=>{
            return(
                <tr>
                    <th >{key+1}</th>
                    <th ><Link href={"/channel/"+res.slug}>{res.name}</Link></th>
                    <td> {res.img ?
                        <img src={ApiUrl.photoUrl+res.img} style={{height:'60px',width:'100px'}} />
                        :
                        <img src="/noimage.png" style={{height:'60px',width:'100px'}} />
                    } </td>
                    <td >
                        {res.email}
                    </td>
                    <th >{res.phone}</th>
                    <th >{res.gender}</th>
                    <th >{res.role}</th>
                    <th >{res.status=="1"?"Active":"Inactive"}</th>
                    <td >{res.ip_created}</td>
                    <td >{res.ip_updated}</td>
                    <td>{moment(res.date).format("MMM Do YY")}</td>
                    <td style={{width:'120px'}}>
                        <Link href={"/admin/user/"+res.id}>
                            <a className="btn btn-success btn-sm">
                                Edit
                            </a>
                        </Link>
                        {res.role=="Admin"?"":
                            <button onClick={this.onDel.bind(this,res.id)} className="btn btn-danger btn-sm">
                            Delete
                        </button>}
                    </td>
                </tr>
            )
        })



        const sdata = this.state.suser.map((res,key)=>{
            return(
                <tr>
                    <th >{key+1}</th>
                    <th ><Link href={"/channel/"+res.slug}>{res.name}</Link></th>
                    <td> {res.img ?
                        <img src={ApiUrl.photoUrl+res.img} style={{height:'60px',width:'100px'}} />
                        :
                        <img src="/noimage.png" style={{height:'60px',width:'100px'}} />
                    } </td>
                    <td >
                        {res.email}
                    </td>
                    <th >{res.phone}</th>
                    <th >{res.gender}</th>
                    <th >{res.role}</th>
                    <th >{res.status=="1"?"Active":"Inactive"}</th>
                    <td >{res.ip_created}</td>
                    <td >{res.ip_updated}</td>
                    <td>{moment(res.date).format("MMM Do YY")}</td>
                    <td style={{width:'120px'}}>
                        <Link href={"/admin/user/"+res.id}>
                            <a className="btn btn-success btn-sm">
                                Edit
                            </a>
                        </Link>
                        <button onClick={this.onDel.bind(this,res.id)} className="btn btn-danger btn-sm">
                            Delete
                        </button>
                    </td>
                </tr>
            )
        })

        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Head>
                        <title>All User </title>
                    </Head>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <Head>
                        <title>All User </title>
                    </Head>
                    <div className="container-fluid bg-white">
                        <div className="row mb-5 post_create">
                            <h3 className="col-md-12 p-3 post_head mb-3">All user
                                ({this.state.suser.length>0?this.state.suser.length:this.state.user.length})
                            </h3>
                            {this.state.user.length>0 ?
                                <>
                                <div className="col-md-6 col-lg-6 col-8 search">
                                    <div className="input-group mb-3 ssection">
                                        <input type="text" className="form-control" placeholder=" Filter by type"
                                               style={{height: '30px'}}
                                               onChange={this.name} value={this.state.name}
                                               aria-label="Recipient's username" aria-describedby="basic-addon2"/>
                                        <div className="input-group-append">
                                            <button onClick={this.submit} style={{height: '30px'}}
                                                    className="btn btn-light searchbtn" title="Search" type="button"><i
                                                className="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="table-responsive">
                                <table className="table " style={{width: '100%'}}>
                                <tbody>
                                <tr>
                                <th scope="col">Sl</th>
                                <th scope="col">Name</th>
                                <th scope="col">Image</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Gender</th>
                                <th scope="col">Role</th>
                                <th scope="col">Status</th>
                                <th scope="col">IP Creted</th>
                                <th scope="col">IP Last</th>
                                <th scope="col">Date</th>
                                <th scope="col">Action</th>
                                </tr>
                                {this.state.suser.length > 0 ? sdata : data}
                                </tbody>
                                </table>
                                </div>
                                </>
                                :
                                <h6 className="text-center mt-5 mb-5">Not avilable</h6>
                            }

                        </div>
                    </div>
                </ContentMain>
            );
        }


    }
}

export default Index;