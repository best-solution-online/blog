import React, {Component} from 'react';
import ContentMain from "../../component/content/ContentMain";
import Head from 'next/head'
import Router from "next/router";
import axios from "axios";
import ApiUrl from "../../component/AppApiUrl/ApiUrl";
import {toast} from "react-toastify";


class Create extends Component {
    constructor() {
        super();
        this.state={
            token:'',link:'',des:'',ads_limit:'0',img:'',img1:'',amount:'',trx:'',sender:'',reciever:'',bank:'',
            ads_list:[],rate:'',
            ads_limitSettings:'0',accSettings:'',title:'',desSettings:'',

        }
    }

    amount=(e)=>{
        var ads_limit = parseInt(e.target.value) * parseInt(this.state.rate)
        this.setState({amount:e.target.value,ads_limit:ads_limit})
    }
    trx=(e)=>{
        this.setState({trx:e.target.value})
    }
    sender=(e)=>{
        this.setState({sender:e.target.value})
    }
    reciever=(e)=>{
        this.setState({reciever:e.target.value})
    }
    link=(e)=>{
        this.setState({link:e.target.value})
    }
    des=(e)=>{
        this.setState({des:e.target.value})
    }

    pay=(e)=>{
        const form = new FormData()
        form.append('id',e.target.value)
        axios.post(ApiUrl.AdsSettingOne,form)
            .then(res=>{
                console.log(res)
                this.setState({ads_limitSettings:res.data[0]['ads_limit'],reciever:res.data[0]['acc'],
                    title:res.data[0]['title'],desSettings:res.data[0]['des'],bank:res.data[0]['title'],rate:res.data[0]['ads_limit']})
            })
            .catch(error=>{
                this.setState({ads_limitSettings:'',accSettings:'',
                    title:'',desSettings:'',bank:'',rate:''})
            })
    }

    img=(e)=>{
        this.setState({img:e.target.files[0]})
        var file = e.target.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function (e) {
            this.setState({
                img1: [reader.result]
            })
        }.bind(this);
    }
    componentDidMount() {
        const token = localStorage.token
        if(token){
            this.setState({token:token})
            axios.post(ApiUrl.AdsSettingAll)
                .then(res=>{
                    console.log(res)
                   this.setState({ads_list:res.data})
                })
                .catch(error=>{
                    //localStorage.removeItem('token')
                    console.log(error)
                })
        }else{
            Router.push('/')
        }
    }

    FormSubmit=(event)=>{
        const formData = new FormData()
        formData.append('bank',this.state.bank)
        formData.append('des',this.state.des)
        formData.append('link',this.state.link)
        formData.append('amount',this.state.amount)
        formData.append('sender',this.state.sender)
        formData.append('reciever',this.state.reciever)
        formData.append('img',this.state.img)
        formData.append('trx',this.state.trx)
        formData.append('ads_limit',this.state.ads_limit)
        formData.append('rate',this.state.rate)
        formData.append('token',this.state.token)
        axios.post(ApiUrl.AdsCreate, formData)
            .then(res=>{
                if(res.data.success){
                    toast.success(res.data.success, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    setTimeout(
                        () => Router.push(`/advertising`),
                        1000
                    )
                }else{
                    toast.error(res.data.error, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
            })
            .catch(error=>{
                console.log(error)
            })
        event.preventDefault()
    }

    render() {
        const ads = this.state.ads_list.map(res=>{
            return(
                <option value={res.id}>{res.title}</option>
            )
        })
        return (
            <ContentMain>
                <Head>
                    <title>Ads Create</title>
                </Head>

                <div className="container-fluid">
                    <form onSubmit={this.FormSubmit}>
                        <div className="row  mb-5 post_create">
                            <h3 className="col-md-12 p-3 post_head mb-3">Create Ads</h3>
                            <div className="col-md-6">

                                    <div className="form-group row">
                                        <label htmlFor="inputPassword3"
                                               className="col-sm-12 col-form-label">Description (required)</label>
                                        <div className="col-sm-12">
                                            <textarea required rows="6" value={this.state.des} onChange={this.des} className="form-control"></textarea>
                                        </div>
                                    </div>
                                <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-12 col-form-label"> Link url (required)</label>
                                    <div className="col-sm-12">
                                        <input onChange={this.link} value={this.state.link} required type="text" className="form-control " id="inputEmail3"
                                               placeholder="Url Here"/>
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label htmlFor="inputPassword3"
                                           className="col-sm-12 col-form-label">Thumbnail Image</label>
                                    <div className="col-sm-12">
                                        <input className="form-control" onChange={this.img} type="file" />
                                    </div>
                                    <div className="col-sm-12">
                                        {this.state.img1?<img id="showImage" className="mt-3 mb-3" style={{height:'200px',width:'250px'}} src={this.state.img1} />:
                                            ''
                                        }
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-12 col-form-label">Payment Method</label>
                                    <div className="col-sm-12">
                                        <select id="inputState" onChange={this.pay} className="form-control">
                                            <option value="" selected >Select Payment Method</option>
                                            {ads}
                                        </select>
                                    </div>
                                </div>

                            </div>
                            {this.state.title == "" ?"":
                                <div className="col-md-6">
                                    <div className="form-group row">
                                        <label htmlFor="inputEmail3" className="col-sm-12 col-form-label"> Amount (required)</label>
                                        <div className="col-sm-12">
                                            <input onChange={this.amount} value={this.state.amount} required type="number" className="form-control " id="inputEmail3"
                                                   placeholder="Amount Here"/>
                                        </div>
                                        <label htmlFor="inputEmail3" className="col-sm-12 col-form-label"> Average Impression {this.state.ads_limit}</label>
                                    </div>

                                    <div className="form-group row">
                                        <label htmlFor="inputEmail3" className="col-sm-12 col-form-label"> Account {this.state.title} </label>
                                        <div className="col-sm-12">
                                            <input  value={this.state.reciever} readOnly type="text" className="form-control " id="inputEmail3"
                                            />
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label htmlFor="inputEmail3" className="col-sm-12 col-form-label"> Sender Account </label>
                                        <div className="col-sm-12">
                                            <input onChange={this.sender} value={this.state.sender}  type="text" className="form-control " id="inputEmail3"
                                                   placeholder="Your Sender account Here"/>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label htmlFor="inputEmail3" className="col-sm-12 col-form-label"> Trx ID</label>
                                        <div className="col-sm-12">
                                            <input onChange={this.trx} value={this.state.trx} required type="text" className="form-control " id="inputEmail3"
                                                   placeholder=" Transaction id Here"/>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <div className="col-sm-12">
                                            <button type="submit" className="btn btn-primary mt-3" style={{float:'right'}}>Submit</button>
                                        </div>
                                    </div>
                                </div>
                            }
                        </div>
                    </form>
                </div>

            </ContentMain>
        );
    }
}

export default Create;