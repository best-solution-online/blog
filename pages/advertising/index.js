import React, {Component} from 'react';
import ContentMain from "../../component/content/ContentMain";
import Head from 'next/head'
import Router from "next/router";
import axios from "axios";
import ApiUrl from "../../component/AppApiUrl/ApiUrl";
import moment from "moment";
import Link from 'next/link'
import {toast} from "react-toastify";
import Loading from "../../component/loading/Loading";

class Index extends Component {
    constructor() {
        super();
        this.state={
            token:'',ads:[],loading:true
        }
    }
    componentDidMount() {
        const token = localStorage.token
        if(token){
            this.setState({token:token})
            const FormDa = new FormData()
            FormDa.append('token',token)
            axios.post(ApiUrl.AdsMy,FormDa)
                .then(res=>{
                    console.log(res)
                    this.setState({ads:res.data,loading:false})
                })
                .catch(error=>{
                    this.setState({loading:false})
                    console.log(error)
                })
        }else{
            Router.push('/')
        }
    }

    onDel=(id)=>{
        var confirm = window.confirm("Are you sure to delete ")
        const FormDa = new FormData()
        FormDa.append('token',this.state.token)
        FormDa.append('id',id)

        if(confirm){
            axios.post(ApiUrl.AdsDelete,FormDa)
                .then(res=>{
                    if(res.data.success){
                        this.componentDidMount()
                        toast.success(res.data.success, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });

                    }else{
                        toast.error(res.data.error, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });

                    }
                })
                .catch(error=>{
                    console.log(error)
                })
        }
    }
    render() {
        const data = this.state.ads.map((res,key)=>{
            return(
                <tr>
                    <th >{key+1}</th>
                    <td> {res.img ? <img src={ApiUrl.photoUrl+res.img} style={{height:'60px',width:'100px'}} /> :
                        <img src="/noimage.png" style={{height:'60px',width:'100px'}} />
                    } </td>
                    <td style={{width:'300px'}}>{res.des.substring(0,100)}...</td>
                    <th >{res.views}</th>
                    <th>{res.click}</th>
                    <th>{res.ads_limit}</th>
                    {res.status =="Active"? <th className="text-primary">{res.status}</th> :<th className="text-danger">{res.status}</th>}
                    <td>{moment(res.date).format("MMM Do YY")}</td>
                    <td>
                        <Link href={"/advertising/edit/"+res.id}>
                            <button className="btn btn-success btn-sm">
                                Edit
                            </button>
                        </Link>
                        <button onClick={this.onDel.bind(this,res.id)} className="btn btn-danger btn-sm">
                            Delete
                        </button>
                    </td>
                </tr>
            )
        })

        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Head>
                        <title> Your Ads List</title>
                    </Head>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <Head>
                        <title> Your Ads List</title>
                    </Head>
                    <div className="container-fluid bg-white">
                        <div className="row mb-5 post_create">
                            <h3 className="col-md-12 p-3 post_head mb-3">Your ads list {this.state.ads.length} </h3>
                            {this.state.ads.length>0?
                                <div className="table-responsive">
                                    <Link href="/advertising/create"><a className="btn btn-primary"> Create
                                        Ads </a></Link>
                                    <table className="table " style={{width: '100%'}}>
                                        <tbody>
                                        <tr>
                                            <th scope="col">Sl</th>
                                            <th scope="col">Image</th>
                                            <th scope="col">Description</th>
                                            <th scope="col">Impression</th>
                                            <th scope="col">Clicked</th>
                                            <th scope="col">Impression Limit</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Date</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        {data}
                                        </tbody>
                                    </table>
                                </div>
                                :
                                <h6 className="text-center mt-5 mb-5">Not avilable</h6>
                            }
                        </div>
                    </div>
                </ContentMain>
            );
        }


    }
}

export default Index;