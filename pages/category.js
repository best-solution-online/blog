import React, {Component} from 'react';
import Head from "next/head";
import Link from "next/link";
import moment from "moment";
import ContentMain from "../component/content/ContentMain";
import axios from "axios";
import ApiUrl from "../component/AppApiUrl/ApiUrl";

class Category extends Component {
    constructor() {
        super();
        this.state={
            cat:[],loading:true
        }
    }

    componentDidMount() {
        axios.post(ApiUrl.CategoryAll)
            .then(res=>{
                console.log(res)
                this.setState({cat:res.data,loading:false})
            })
            .catch(error=>{
                //localStorage.removeItem('token')
                console.log(error)
            })
    }
    render() {
        return (
            <ContentMain>
                <div className="container-fluid">
                    <Head>
                        <title>Category </title>
                    </Head>

                    <div className="cate row">
                        {this.state.cat.map(res=>{
                            return(
                                <div className="vertical-menu">
                                    <Link href="#"><a  >{res.category}</a></Link>
                                </div>
                            )
                        })}
                    </div>

                </div>
            </ContentMain>
        );
    }
}

export default Category;
