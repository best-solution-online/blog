import React, {Component} from 'react';
import ContentMain from "../../../component/content/ContentMain";
import ApiUrl from "../../../component/AppApiUrl/ApiUrl";
import Link from 'next/link'
import Router from "next/router";
import axios from 'axios'
import Head from 'next/head'
import Loading from "../../../component/loading/Loading";
import jwtDecode from 'jwt-decode'
import moment from 'moment'
import {toast} from "react-toastify";

export async function getServerSideProps({query}) {
    return {
        props: {query}
    }
}
class About extends Component {
    constructor() {
        super();
        this.state={
            path:'',name:'',email:'',img:'',cover:'',

            f_status:'',s_status:'',created_at:'',facebook:'',des:'',post:[],
            twitter:'',id:'',myid:'',youtube:'',instagram:'',linkedin:'',followers:'0',

            follow:'',token:'',slug:'',loading:true
        }
    }

    componentDidMount() {
        this.setState({path:Router.pathname})
        const token = localStorage.token
        if(token){
            const decoded = jwtDecode(token)
            this.setState({token:token,myid:decoded.uid})
        }
        const myFormData = new FormData()
        myFormData.append('slug',this.props.query.slug)

        axios.post(ApiUrl.UserInfoProfile,myFormData)
            .then(res=>{
                this.setState({name:res.data.info[0]['name'],img:res.data.info[0]['img'],

                    f_status:res.data.info[0]['f_status'],s_status:res.data.info[0]['s_status'],created_at:res.data.info[0]['created_at'],
                    facebook:res.data.info[0]['facebook'],instagram:res.data.info[0]['instagram'],linkedin:res.data.info[0]['linkedin'],
                    des:res.data.info[0]['des'], id:res.data.info[0]['id'],email:res.data.info[0]['email'],
                    twitter:res.data.info[0]['twitter'],youtube:res.data.info[0]['youtube'],post:res.data.post,

                    cover:res.data.info[0]['cover'],follow:res.data.info[0]['follow'],slug:res.data.info[0]['slug'],
                    role:res.data.info[0]['role'],})
                const formData2 = new FormData()
                formData2.append('channel',res.data.info[0]['id'])
                formData2.append('token',token)
                axios.post(ApiUrl.FollowCheck, formData2)
                    .then(res=>{
                        console.log(res)
                        if(res.data.success){
                            this.setState({followers:'1', loading:false})
                        }else{
                            this.setState({followers:'0', loading:false})
                        }
                    })
                    .catch(error=>{
                        console.log(error)
                        this.setState({loading:false})
                    })


            })
            .catch(error=>{
                console.log(error)
            })
    }

    onFollow=(channel)=>{
        const token = this.state.token;
        if(token == ""){
            toast.error("Login is required", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }else{
            const formData2 = new FormData()
            formData2.append('channel',this.state.id)
            formData2.append('token',token)
            axios.post(ApiUrl.FollowCreate, formData2)
                .then(res=>{
                    //this.componentDidMount()
                    console.log(res)
                    if(res.data.success){
                        console.log(res.data.success)
                        this.setState({followers:res.data.success})
                        this.componentDidMount()
                    }else{
                        console.log("Error Follow")
                    }
                })
                .catch(error=>{
                    console.log(error)
                })
        }
    }

    render() {

        var view = 0;
        const post = this.state.post.map(res=>{
            return view+=parseFloat(res.views)
        })

        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <Head>
                        <title>{this.state.name}</title>
                    </Head>
                    {this.state.cover ? <img src={ApiUrl.photoUrl+this.state.cover} className="cover" /> :
                        <img className="cover" src='/cover.png'/>
                    }
                    <div className="row cover_row" >
                        <div className="col-md-6 col-12 ">
                            <div className="row mt-4">
                                <div className="col-md-3 col-3">
                                    {this.state.img ? <img className="acc_img" src={ApiUrl.photoUrl+this.state.img}/>:
                                        <img className="acc_img" src="/profile.png" />
                                    }
                                </div>
                                <div className="col-md-7 col-7 mt-1">
                                    <h3 style={{marginBottom:'0px'}}>{this.state.name}</h3>
                                    {this.state.f_status=='1' || this.state.id == this.state.myid ?<p >{this.state.follow} Followers</p>:''}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-12 p-md-5 p-3">
                            {this.state.id == this.state.myid?<>
                                <Link href='/settings'><a className="btn btn-primary" style={{marginRight:'10px'}}>CUSTOMIZE PROFILE</a></Link>
                                <Link href='/post'><a className="btn btn-primary">MANAGE POST</a></Link>
                            </>:this.state.followers == "1" ? <button onClick={this.onFollow} className="btn btn-white followbtn float-end" >FOLLOWED</button>:
                                <button onClick={this.onFollow} className="btn btn-danger followbtnactive float-end" >FOLLOW</button>}
                        </div>
                        <div className="col-md-12 col-12 acc_list">
                            <Link href={"/channel/"+this.state.slug} ><a className={this.state.path=="/channel/[slug]" ?"active":''}>
                                HOME </a></Link>
                            <Link href={"/channel/"+this.state.slug+"/favorite"} ><a className={this.state.path=="/channel/[slug]/favorite"?"active":''}>  FAVORITE </a></Link>
                            <Link href={"/channel/"+this.state.slug+"/follows"} ><a className={this.state.path=="/channel/[slug]/follows"?"active":''}>  FOLLOWS </a></Link>
                            <Link href={"/channel/"+this.state.slug+"/about" }><a className={this.state.path=="/channel/[slug]/about"?"active":''}>  ABOUT </a></Link>
                        </div>
                    </div>

                    <div className="container-fluid">
                        <div className="row post_row" >
                          <div className="col-md-8 col-12 pt-5 pr-3">
                             <h5>Description</h5>
                              <p>{this.state.des}</p>
                              <hr style={{width:'100%'}}/>

                              {this.state.s_status=="1" || this.state.id == this.state.myid ?
                                  <div className="about">
                                      <h5>Details</h5>
                                      {this.state.email ? <p>Email : {this.state.email}</p>:''}

                                      {this.state.facebook ? <a href={this.state.facebook} target="blank">
                                          <i className="fab fa-facebook text-primary"></i>
                                      </a> :''}
                                      {this.state.twitter ? <a href={this.state.twitter} target="blank">
                                          <i className="fab fa-twitter text-primary"></i>
                                      </a> :''}
                                      {this.state.youtube ? <a href={this.state.youtube} target="blank">
                                          <i className="fab fa-youtube text-danger"></i>
                                      </a> :''}
                                      {this.state.instagram ? <a href={this.state.instagram} target="blank">
                                          <i className="fab fa-instagram text-primary"></i>
                                      </a> :''}
                                      {this.state.linkedin ? <a href={this.state.linkedin} target="blank">
                                          <i className="fab fa-linkedin text-primary"></i>
                                      </a> :''}
                                      <br/>
                                      <br/>
                                      <hr style={{width:'100%'}}/>
                                  </div>
                                  :''
                              }
                          </div>
                            <div className="col-md-4 col-12 pt-5">
                                  <h5>Stats</h5>
                                  <hr style={{width:'100%'}} />
                                <p>Joined {moment(this.state.created_at).format("MMM Do YY")} </p>
                                <hr style={{width:'100%'}} />
                                <p>{view} views</p>
                                <hr style={{width:'100%'}} />
                            </div>
                        </div>
                    </div>


                </ContentMain>
            );
        }

    }
}

export default About;