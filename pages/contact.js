import React, {Component} from 'react';
import {DataContext} from "../context/ContextApi";
import ContentMain from "../component/content/ContentMain";
import Head from 'next/head'
class Contact extends Component {
    static contextType = DataContext;
    render() {
        const {contact,title} = this.context;
        return (
            <ContentMain>
                <Head>
                    <title>Contact Us {title}</title>
                </Head>
                <div className="container">
                    <div className="row m-2 mt-3">
                        <div className="col-md-12 p-md-5 p-col-3 ">
                            <h3 className="text-center mb-3 mt-2">Contact US</h3>
                            {contact}
                        </div>
                    </div>
                </div>
            </ContentMain>
        );
    }
}

export default Contact;