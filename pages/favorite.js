import React, {Component} from 'react';
import axios from "axios";
import ApiUrl from "../component/AppApiUrl/ApiUrl";
import Router from "next/router";
import {toast} from "react-toastify";
import moment from "moment";
import Link from "next/link";
import ContentMain from "../component/content/ContentMain";
import Head from "next/head";
import Loading from "../component/loading/Loading";

class Favorite extends Component {
    constructor() {
        super();
        this.state={
            token:'',favorite:[],loading:true
        }
    }
    componentDidMount() {
        const token = localStorage.token
        if(token){
            this.setState({token:token})
            const FormDa = new FormData()
            FormDa.append('token',token)
            axios.post(ApiUrl.FavoriteMy,FormDa)
                .then(res=>{
                    console.log(res)
                    this.setState({favorite:res.data,loading:false})
                })
                .catch(error=>{
                    console.log(error)
                })
        }else{
            Router.push('/')
        }
    }

    onDel=(id)=>{
        var confirm = window.confirm("Are you sure to delete ")
        const FormDa = new FormData()
        FormDa.append('token',this.state.token)
        FormDa.append('id',id)

        if(confirm){
            axios.post(ApiUrl.FavoriteDelete,FormDa)
                .then(res=>{
                    if(res.data.success){
                        this.componentDidMount()
                        toast.success(res.data.success, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });

                    }else{
                        toast.error(res.data.error, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });

                    }
                })
                .catch(error=>{
                    console.log(error)
                })
        }
    }
    render() {
        const data = this.state.favorite.map((res,key)=>{
            return(
                <tr>
                    <th >{key+1}</th>
                    <td> {res.img ? <img src={ApiUrl.photoUrl+res.img} style={{height:'60px',width:'100px'}} /> :
                        <img src="/noimage.png" style={{height:'60px',width:'100px'}} />
                    } </td>
                    <td style={{width:'200px'}}>{res.title.substring(0,70)}...</td>
                    <td style={{width:'300px'}}>{res.body.substring(0,100)}...</td>
                    <td>
                        <Link href={"/watch?p="+res.slug}>
                            <button className="btn btn-info btn-sm">
                                View
                            </button>
                        </Link>
                        <button onClick={this.onDel.bind(this,res.id)} className="btn btn-danger btn-sm">
                            Delete
                        </button>
                    </td>
                </tr>
            )
        })
        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Head>
                        <title> Favorite List</title>
                    </Head>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <Head>
                        <title> Favorite List</title>
                    </Head>
                    <div className="container-fluid bg-white">
                        <div className="row mb-5 post_create">
                            <h3 className="col-md-12 p-3 post_head mb-3">Channel Favorite content {this.state.favorite.length}</h3>
                            {this.state.favorite.length>0 ?
                                <div className="table-responsive">
                                <table className="table " style={{width:'100%'}}>
                                    <tbody>
                                    <tr>
                                        <th scope="col">Sl</th>
                                        <th scope="col">Image</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    {data}
                                    </tbody>
                                </table>
                            </div>
                                :
                                <h6 className="text-center mt-5 mb-5">Not avilable</h6>
                            }

                        </div>
                    </div>
                </ContentMain>
            );
        }

    }
}

export default Favorite;