import Head from 'next/head'
import ContentMain from "../component/content/ContentMain";
import React, {Component} from 'react';
import Link from "next/link";
import moment from "moment";
import axios from "axios";
import ApiUrl from "../component/AppApiUrl/ApiUrl";
import Loading from "../component/loading/Loading";
import {DataContext} from "../context/ContextApi";
import Moment from 'react-moment';
// next build , next export

class Home extends Component {
    static contextType = DataContext;
    constructor() {
        super();
        this.state={
            post:[],ads:[],
            loading:true
        }
    }

    componentDidMount() {
        axios.post(ApiUrl.AdsView)
            .then(res=>{
                console.log(res)
                this.setState({ads:res.data})
            })
            .catch(error=>{
                console.log(error)
            })
        axios.post(ApiUrl.PostAll)
            .then(res=>{
                console.log(res)
                this.setState({post:res.data,loading:false})
            })
            .catch(error=>{
                console.log(error)
            })
    }

    render() {
        const {title,adsClick} = this.context
        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Head>
                        <title> {title} </title>
                    </Head>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <div className="container-fluid">
                        <Head>
                            <title> {title} </title>
                        </Head>
                        <div className="row post_row" >

                            {this.state.ads.map(res=>{
                                return(
                                    <>
                                        <div className="col-md-6 col-12">
                                                <a href={res.link} target="blank" onClick={adsClick.bind(res.id)}>
                                                    {res.img ?
                                                        <img className="d-block w-100 bannerimg"
                                                             src={ApiUrl.photoUrl+res.img} alt="First slide"/>
                                                        :
                                                        <img className="d-block w-100 bannerimg"
                                                             src="/noimage.png" alt="First slide"/>
                                                    }
                                                </a>
                                        </div>
                                        <div className="col-md-6 col-12 banner_content">
                                            <a onClick={adsClick(res.id)} href={res.link} target="blank"> {res.des.substring(0,600)}... </a>
                                        </div>
                                    </>
                                )
                            })}


                            {this.state.post.map(res=>{
                                return(
                                    <div className="col-md-3 col-12 text-dark  pt-3">
                                        <Link href={{pathname:'watch',query:{p:res.slug}}}>
                                            <a>{res.img ? <img className="post_img"
                                                               src={ApiUrl.photoUrl+res.img}/>:
                                                <img className="post_img"
                                                     src="/noimage.png"/>
                                            }</a>
                                        </Link>
                                        <div className="row">
                                            <div className="col-md-2 col-2">
                                                <Link href={"/channel/"+res.u_slug}>
                                                    <a >{res.u_img ? <img className="puserpic"
                                                                         src={ApiUrl.photoUrl+res.u_img}/>:
                                                        <img className="puserpic"
                                                             src="/profile.png"/>}</a>
                                                </Link>
                                            </div>
                                            <div className="col-md-10 col-10">
                                                <Link href={{pathname:'watch',query:{p:res.slug}}}>
                                                    <a><h6 className="pt-2 ptitle">{res.title.substring(0,60)}...</h6></a>
                                                </Link>
                                                <Link href={"/channel/"+res.u_slug}>
                                                    <a ><p className="pt-2 puname">{res.u_name}</p></a>
                                                </Link>
                                                <Link href={{pathname:'watch',query:{p:res.slug}}}>
                                                    <a><p className="pt-2 views">{res.views} views. {<Moment fromNow>{res.created_at}</Moment>}</p></a>
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}

                        </div>
                    </div>
                </ContentMain>
            );
        }
    }
}

export default Home;
