import React, {Component} from 'react';
import Head from "next/head";
import Link from "next/link";
import moment from "moment";
import ContentMain from "../component/content/ContentMain";
import axios from "axios";
import ApiUrl from "../component/AppApiUrl/ApiUrl";
import Loading from "../component/loading/Loading";
import {DataContext} from "../context/ContextApi";
import Moment from 'react-moment';

class Popular extends Component {
    static contextType = DataContext;
    constructor() {
        super();
        this.state={
            post:[],ads:[],
            loading:true
        }
    }

    componentDidMount() {
        axios.post(ApiUrl.AdsView)
            .then(res=>{
                console.log(res)
                this.setState({ads:res.data})
            })
            .catch(error=>{
                console.log(error)
            })
        axios.post(ApiUrl.PostPopular)
            .then(res=>{
                console.log(res)
                this.setState({post:res.data,loading:false})
            })
            .catch(error=>{
                //localStorage.removeItem('token')
                console.log(error)
            })
    }

    render() {
        const {adsClick} = this.context

        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Head>
                        <title> Popular Post </title>
                    </Head>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <div className="container-fluid">
                        <Head>
                            <title> Popular Post </title>
                        </Head>
                        <div className="row post_row" >

                            {this.state.ads.map(res=>{
                                return(
                                    <>
                                        <div className="col-md-6 col-12">
                                            <a href={res.link} target="blank" onClick={adsClick.bind(res.id)}>
                                                {res.img ?
                                                    <img className="d-block w-100 bannerimg"
                                                         src={ApiUrl.photoUrl+res.img} alt="First slide"/>
                                                    :
                                                    <img className="d-block w-100 bannerimg"
                                                         src="/noimage.png" alt="First slide"/>
                                                }
                                            </a>
                                        </div>
                                        <div className="col-md-6 col-12 banner_content">
                                            <a href={res.link} target="blank" onClick={adsClick.bind(res.id)}> {res.des.substring(0,600)}... </a>
                                        </div>
                                    </>
                                )
                            })}


                            {this.state.post.map(res=>{
                                return(
                                    <>
                                        <div className="col-md-4 col-12 text-dark  pt-3">
                                            <Link href={{pathname:'watch',query:{p:res.slug}}}>
                                                <a>{res.img ? <img className="post_img"
                                                                   src={ApiUrl.photoUrl+res.img}/>:
                                                    <img className="post_img"
                                                         src="/noimage.png"/>
                                                }</a>
                                            </Link>

                                        </div>
                                        <div className="col-md-8">
                                            <div className="row">
                                                <div className="col-md-12 col-12">
                                                    <Link href={{pathname:'watch',query:{p:res.slug}}}>
                                                        <a><h6 className="mt-3 ptitle ">{res.title.substring(0,100)}...</h6></a>
                                                    </Link>
                                                    <Link href={{pathname:'watch',query:{p:res.slug}}}>
                                                        <a><p className=" views">{res.views} views. {<Moment fromNow>{res.created_at}</Moment>}</p></a>
                                                    </Link>
                                                    <Link href={"/channel/"+res.u_slug}>
                                                        <a><p className="" style={{marginBottom:'0'}}>{res.u_img ?

                                                            <img style={{height:'30px',width:'30px',borderRadius:'50%',marginRight:'5px'}}
                                                                 src={ApiUrl.photoUrl+res.u_img}/>:
                                                            <img style={{height:'30px',width:'30px',borderRadius:'50%',marginRight:'5px'}}
                                                                 src="/profile.png"/>}{res.u_name}</p></a>
                                                    </Link>
                                                    <Link href={{pathname:'watch',query:{p:res.slug}}}>
                                                        <a><p className="ptitle">{res.body.substring(0,150)}...</p></a>
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                )
                            })}

                        </div>
                    </div>
                </ContentMain>
            );
        }
    }
}

export default Popular;
