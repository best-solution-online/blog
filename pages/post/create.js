import React, {Component} from 'react';
import ContentMain from "../../component/content/ContentMain";
import Head from 'next/head'
import Router from "next/router";
import axios from "axios";
import ApiUrl from "../../component/AppApiUrl/ApiUrl";
import {toast} from "react-toastify";
import jwtDecode from 'jwt-decode'

class Create extends Component {
    constructor() {
        super();
        this.state={
            token:'',title:'',body:'',tags:'',category:'Default',com_status:'On',
            meta:'',status:'Public',img:'',img1:'',video:'',catList:[],u_status:''
        }
    }
    com_status=(e)=>{
        this.setState({com_status:e.target.value})
    }
    title=(e)=>{
        this.setState({title:e.target.value})
    }
    body=(e)=>{
        this.setState({body:e.target.value})
    }
    tags=(e)=>{
        this.setState({tags:e.target.value})
    }
    category=(e)=>{
        this.setState({category:e.target.value})
    }
    meta=(e)=>{
        this.setState({meta:e.target.value})
    }
    status=(e)=>{
        this.setState({status:e.target.value})
    }
    img=(e)=>{
        this.setState({img:e.target.files[0]})
        var file = e.target.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function (e) {
            this.setState({
                img1: [reader.result]
            })
        }.bind(this);
    }
    componentDidMount() {
        const token = localStorage.token
        if(token){
            this.setState({token:token})
            const formData = new FormData()
            formData.append('token',token)
            axios.post(ApiUrl.ProfileAuth,formData)
                .then(res=>{
                    console.log(res)
                    this.setState({u_status:res.data.info[0]['status']})
                })
                .catch(error=>{
                    console.log(error)
                })

            axios.post(ApiUrl.CategoryAll)
                .then(res=>{
                    console.log(res)
                   this.setState({catList:res.data})
                })
                .catch(error=>{
                    //localStorage.removeItem('token')
                    console.log(error)
                })
        }else{
            Router.push('/')
        }
    }

    FormSubmit=(event)=>{
        if(this.state.u_status=="1"){
            const formData = new FormData()
            formData.append('title',this.state.title)
            formData.append('body',this.state.body)
            formData.append('meta',this.state.meta)
            formData.append('tags',this.state.tags)
            formData.append('status',this.state.status)
            formData.append('category',this.state.category)
            formData.append('img',this.state.img)
            formData.append('video',this.state.video)
            formData.append('com_status',this.state.com_status)
            formData.append('token',this.state.token)
            axios.post(ApiUrl.PostCreate, formData)
                .then(res=>{
                    if(res.data.success){
                        toast.success(res.data.success, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                        setTimeout(
                            () => Router.push(`/post`),
                            1000
                        )
                    }else{
                        toast.error(res.data.error, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }
                })
                .catch(error=>{
                    console.log(error)
                })
        }else{
            toast.error("Your account inactive ", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }

        event.preventDefault()
    }

    render() {
        const cat = this.state.catList.map(res=>{
            return(
                <option value={res.category}>{res.category}</option>
            )
        })
        return (
            <ContentMain>
                <Head>
                    <title>Post Create</title>
                </Head>

                <div className="container-fluid">
                    <form onSubmit={this.FormSubmit}>
                        <div className="row  mb-5 post_create">
                            <h3 className="col-md-12 p-3 post_head mb-3">Create Post</h3>
                            <div className="col-md-6">
                                    <div className="form-group row">
                                        <label htmlFor="inputEmail3" className="col-sm-12 col-form-label">Title (required)</label>
                                        <div className="col-sm-12">
                                            <input onChange={this.title} value={this.state.title} required type="text" className="form-control " id="inputEmail3"
                                                   placeholder="Title Here"/>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label htmlFor="inputPassword3"
                                               className="col-sm-12 col-form-label">Description (required)</label>
                                        <div className="col-sm-12">
                                            <textarea required rows="8" value={this.state.body} onChange={this.body} className="form-control"></textarea>
                                        </div>
                                    </div>
                                <div className="form-group row">
                                    <label htmlFor="inputPassword3"
                                           className="col-sm-12 col-form-label">SEO Tags</label>
                                    <div className="col-sm-12">
                                        <textarea rows="3" className="form-control" value={this.state.tags} onChange={this.tags} placeholder="tag, tag, tag, tag"></textarea>
                                    </div>
                                </div>


                            </div>
                            <div className="col-md-6">
                                <div className="form-group row">
                                    <label htmlFor="inputPassword3"
                                           className="col-sm-12 col-form-label">Meta Description</label>
                                    <div className="col-sm-12">
                                        <textarea rows="3" className="form-control" value={this.state.meta} onChange={this.meta}></textarea>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label htmlFor="inputEmail3" className="col-sm-12 col-form-label">Category</label>
                                    <div className="col-sm-12">
                                        <select id="inputState" onChange={this.category} className="form-control">
                                            <option value="Default" selected >Select Category</option>
                                            {cat}
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label htmlFor="inputPassword3"
                                           className="col-sm-12 col-form-label">Visibility</label>
                                    <div className="col-sm-12">
                                        <select id="inputState" onChange={this.status} className="form-control">
                                            <option selected value="Public" >Public</option>
                                            <option value="Private">Private</option>
                                        </select>
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label htmlFor="inputPassword3"
                                           className="col-sm-12 col-form-label">Comment Visibility</label>
                                    <div className="col-sm-12">
                                        <select id="inputState" onChange={this.com_status} className="form-control">
                                            <option selected value="On" >On</option>
                                            <option value="Off">Off</option>
                                        </select>
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label htmlFor="inputPassword3"
                                           className="col-sm-12 col-form-label">Thumbnail Image</label>
                                    <div className="col-sm-12">
                                        <input className="form-control" onChange={this.img} type="file" />
                                    </div>
                                    <div className="col-sm-12">
                                        {this.state.img1?<img id="showImage" className="mt-3 mb-3" style={{height:'200px',width:'250px'}} src={this.state.img1} />:
                                            ''
                                        }
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <div className="col-sm-12">
                                        <button type="submit" className="btn btn-primary mt-3" style={{float:'right'}}>Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </ContentMain>
        );
    }
}

export default Create;