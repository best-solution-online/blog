import React, {Component} from 'react';
import ContentMain from "../../../component/content/ContentMain";
import Head from 'next/head'
import axios from 'axios'
import ApiUrl from "../../../component/AppApiUrl/ApiUrl";
import Loading from "../../../component/loading/Loading";
import {toast} from "react-toastify";
import Router from 'next/router'
import jwtDecode from 'jwt-decode'

export async function getServerSideProps({query}) {
    const s = "Ok"
    return {
        props: {query}
    }

}

class Pid extends Component {

    constructor() {
        super();
        this.state={
            token:'',title:'',body:'',tags:'',category:'',meta:'',com_status:'',role:'',
            status:'',img:'',img1:'',video:'',catList:[],id:'',loading:true
        }
    }
    com_status=(e)=>{
        this.setState({com_status:e.target.value})
    }
    title=(e)=>{
        this.setState({title:e.target.value})
    }
    body=(e)=>{
        this.setState({body:e.target.value})
    }
    tags=(e)=>{
        this.setState({tags:e.target.value})
    }
    category=(e)=>{
        this.setState({category:e.target.value})
    }
    meta=(e)=>{
        this.setState({meta:e.target.value})
    }
    status=(e)=>{
        this.setState({status:e.target.value})
    }
    img=(e)=>{
        this.setState({img:e.target.files[0]})
        var file = e.target.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function (e) {
            this.setState({
                img1: [reader.result]
            })
        }.bind(this);
    }
    componentDidMount() {
        const token = localStorage.token
        if(token){
            var decoded = jwtDecode(token);
            this.setState({token:token,role:decoded.role})
            axios.post(ApiUrl.CategoryAll)
                .then(res=>{
                    console.log(res)
                    this.setState({catList:res.data})
                })
                .catch(error=>{
                    //localStorage.removeItem('token')
                    console.log(error)
                })
            const formData = new FormData()
            formData.append('token',token)
            formData.append('id',this.props.query.pid)
            axios.post(ApiUrl.PostOne,formData)
                .then(res=>{
                    this.setState({id:res.data[0]['id'],category:res.data[0]['category'],
                        title:res.data[0]['title'],body:res.data[0]['body'],tags:res.data[0]['tags'],
                        meta:res.data[0]['meta'],status:res.data[0]['status'],img:res.data[0]['img'],
                        com_status:res.data[0]['com_status'],token:token,loading:false})
                })
                .catch(error=>{
                    console.log(error)
                })
        }else{
            Router.push('/')
        }
    }

    FormSubmit=(event)=>{
        const formData = new FormData()
        formData.append('title',this.state.title)
        formData.append('body',this.state.body)
        formData.append('meta',this.state.meta)
        formData.append('tags',this.state.tags)
        formData.append('status',this.state.status)
        formData.append('category',this.state.category)
        formData.append('img',this.state.img)
        formData.append('video',this.state.video)
        formData.append('id',this.state.id)
        formData.append('com_status',this.state.com_status)
        formData.append('token',this.state.token)
        axios.post(ApiUrl.PostUpdate, formData)
            .then(res=>{
                if(res.data.success){
                    toast.success(res.data.success, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    setTimeout(
                        () => {
                            if(this.state.role=="Admin"){
                                Router.push(`/admin/post`)
                            }else{
                                Router.push(`/post`)
                            }
                        },
                        1000
                    )
                }else{
                    toast.error(res.data.error, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
            })
            .catch(error=>{
                console.log(error)
            })
        event.preventDefault()
    }

    render() {
        const cat = this.state.catList.map(res=>{
            return(
                <option value={res.category}>{res.category}</option>
            )
        })

        if(this.state.loading==true){
            return (
                <ContentMain>
                    <Head>
                        <title>Post Edit</title>
                    </Head>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <Head>
                        <title>Post Update</title>
                    </Head>

                    <div className="container-fluid">
                        <form onSubmit={this.FormSubmit}>
                            <div className="row  mb-5 post_create">
                                <h3 className="col-md-12 p-3 post_head mb-3">Update Post</h3>
                                <div className="col-md-6">
                                    <div className="form-group row">
                                        <label htmlFor="inputEmail3" className="col-sm-12 col-form-label">Title (required)</label>
                                        <div className="col-sm-12">
                                            <input onChange={this.title} value={this.state.title} required type="text" className="form-control " id="inputEmail3"
                                                   placeholder="Title Here"/>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label htmlFor="inputPassword3"
                                               className="col-sm-12 col-form-label">Description (required)</label>
                                        <div className="col-sm-12">
                                            <textarea required rows="8" value={this.state.body} onChange={this.body} className="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label htmlFor="inputPassword3"
                                               className="col-sm-12 col-form-label">SEO Tags</label>
                                        <div className="col-sm-12">
                                            <textarea rows="3" className="form-control" value={this.state.tags} onChange={this.tags} placeholder="tag, tag, tag, tag"></textarea>
                                        </div>
                                    </div>


                                </div>
                                <div className="col-md-6">
                                    <div className="form-group row">
                                        <label htmlFor="inputPassword3"
                                               className="col-sm-12 col-form-label">Meta Description</label>
                                        <div className="col-sm-12">
                                            <textarea rows="3" className="form-control" value={this.state.meta} onChange={this.meta}></textarea>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label htmlFor="inputEmail3" className="col-sm-12 col-form-label">Category</label>
                                        <div className="col-sm-12">
                                            <select id="inputState" onChange={this.category} className="form-control">
                                                {this.state.category ? <option value={this.state.category} selected >{this.state.category}</option>:
                                                    <option value="Default" selected >Select Category</option>
                                                }
                                                {cat}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label htmlFor="inputPassword3"
                                               className="col-sm-12 col-form-label">Visibility</label>
                                        <div className="col-sm-12">
                                            <select id="inputState" onChange={this.status} className="form-control">
                                                <option selected value={this.state.status} >{this.state.status}</option>
                                                <option  value="Public" >Public</option>
                                                <option value="Private">Private</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label htmlFor="inputPassword3"
                                               className="col-sm-12 col-form-label">Comment Visibility</label>
                                        <div className="col-sm-12">
                                            <select id="inputState" onChange={this.com_status} className="form-control">
                                                <option selected value={this.state.com_status}>{this.state.com_status}</option>
                                                <option  value="On" >On</option>
                                                <option value="Off">Off</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label htmlFor="inputPassword3"
                                               className="col-sm-12 col-form-label">Thumbnail Image</label>
                                        <div className="col-sm-12">
                                            <input className="form-control" onChange={this.img} type="file" />
                                        </div>
                                        <div className="col-sm-12">
                                            {this.state.img1?<img id="showImage" className="mt-3 mb-3" style={{height:'200px',width:'250px'}} src={this.state.img1} />:
                                                this.state.img ? <img className="mt-3 mb-3" style={{height:'200px',width:'250px'}} src={ApiUrl.photoUrl+this.state.img}/> :''
                                            }
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <div className="col-sm-12">
                                            <button type="submit" className="btn btn-primary mt-3" style={{float:'right'}}>Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </ContentMain>
            );
        }
    }
}

export default Pid;