import React, {Component} from 'react';
import {DataContext} from "../context/ContextApi";
import ContentMain from "../component/content/ContentMain";
import Head from 'next/head'
class Privacy extends Component {
    static contextType = DataContext;
    render() {
        const {privacy,title} = this.context;
        return (
            <ContentMain>
                <Head>
                    <title>Privacy Policy {title}</title>
                </Head>
                <div className="container">
                    <div className="row m-2 mt-3">
                        <div className="col-md-12 p-md-5 p-col-3  ">
                            <h3 className="text-center mb-3 mt-2">Privacy Policy</h3>
                            {privacy}
                        </div>
                    </div>
                </div>
            </ContentMain>
        );
    }
}

export default Privacy;