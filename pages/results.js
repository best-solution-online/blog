import React, {Component} from 'react';
import ContentMain from "../component/content/ContentMain";
import {DataContext} from "../context/ContextApi";
import Head from "next/head";
import Link from "next/link";
import ApiUrl from "../component/AppApiUrl/ApiUrl";
import moment from "moment";



class Results extends Component {
    static contextType = DataContext;

    static getInitialProps({query}){
        return (
            {query}
        )
    }

    render() {
        const {search,token} = this.context;
        if(search.length>0){
            return (
                <ContentMain>
                    <div className="container-fluid">
                        <Head>
                            <title> Result {this.props.query.search_query}</title>
                        </Head>
                        <div className="row post_row">

                            {search.map(res=>{
                                return(
                                    <>
                                    <div className="col-md-4 col-12 text-dark  pt-3">
                                        <Link href={{pathname:'watch',query:{p:res.slug}}}>
                                            <a>{res.img ? <img className="post_img"
                                                               src={ApiUrl.photoUrl+res.img}/>:
                                                <img className="post_img"
                                                     src="/noimage.png"/>
                                            }</a>
                                        </Link>

                                    </div>
                                        <div className="col-md-8">
                                            <div className="row">
                                                <div className="col-md-12 col-12">
                                                    <Link href={{pathname:'watch',query:{p:res.slug}}}>
                                                        <a><h6 className="mt-3 ptitle ">{res.title.substring(0,100)}...</h6></a>
                                                    </Link>
                                                    <Link href={{pathname:'watch',query:{p:res.slug}}}>
                                                        <a><p className=" views">{res.views} views. {moment(res.created_at, "YYYYMMDD").fromNow()}</p></a>
                                                    </Link>
                                                    <Link href={"/channel/"+res.u_slug}>
                                                        <a><p className="" style={{marginBottom:'0'}}>{res.u_img ?

                                                            <img style={{height:'30px',width:'30px',borderRadius:'50%',marginRight:'5px'}}
                                                                                                        src={ApiUrl.photoUrl+res.u_img}/>:
                                                            <img style={{height:'30px',width:'30px',borderRadius:'50%',marginRight:'5px'}}
                                                                 src="/profile.png"/>}{res.u_name}</p></a>
                                                    </Link>
                                                    <Link href={{pathname:'watch',query:{p:res.slug}}}>
                                                        <a><p className="ptitle">{res.body.substring(0,150)}...</p></a>
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                )
                            })}

                        </div>
                    </div>
                </ContentMain>
            );
        }else{
            return (
                <ContentMain>
                    <Head>
                        <title>No results found</title>
                    </Head>
                    <div className="bg-light p-5 text-center" style={{height:'100%!important'}}>
                        <h2 className="mt-5 pt-5">No results found</h2>
                        <p>Try different keywords or remove search filters</p>
                    </div>
                </ContentMain>
            );
        }
    }
}

export default Results;
