import React, {Component} from 'react';
import ContentMain from "../../component/content/ContentMain";
import Head from "next/head";
import SettingsTop from "../../component/profile/SettingsTop";
import Router from "next/router";
import axios from "axios";
import ApiUrl from "../../component/AppApiUrl/ApiUrl";
import {toast} from "react-toastify";
import {DataContext} from "../../context/ContextApi";
import Loading from "../../component/loading/Loading";

class Index extends Component {
    static contextType = DataContext;
    constructor() {
        super();
        this.state={
            address:'',phone:'',name:'',birth:'',img:'',cover:'',img1:'',cover1:'',gender:'',token:'',des:'',loading:true
        }
    }

    componentDidMount() {
        const token = localStorage.token
        if(token){
            this.setState({token:token})
            const myFormData = new FormData()
            myFormData.append('token',token)
            const config = {
                headers : {'content-type':'multipart/form-data'}
            }
            axios.post(ApiUrl.ProfileAuth,myFormData,config)
                .then(res=>{
                    if(res.data.success){
                        this.setState({name:res.data.info[0]['name'],img:res.data.info[0]['img'],birth:res.data.info[0]['birth'],
                            cover:res.data.info[0]['cover'],address:res.data.info[0]['address'],des:res.data.info[0]['des'],
                            phone:res.data.info[0]['phone'],gender:res.data.info[0]['gender'],token:token,loading:false})
                    }else{
                        console.log("Error")
                    }
                })
                .catch(error=>{
                    //localStorage.removeItem('token')
                    console.log(error)
                })
        }else{
            Router.push('/')
        }
    }

    onName(e){
        this.setState({name:e.target.value})
    }
    onGender(e){
        this.setState({gender:e.target.value})
    }
    onBirth(e){
        this.setState({birth:e.target.value})
    }
    onPhone(e){
        this.setState({phone:e.target.value})
    }
    onAddress(e){
        this.setState({address:e.target.value})
    }
    onDes(e){
        this.setState({des:e.target.value})
    }
    onImg(e){
        this.setState({img:e.target.files[0]})
        var file = e.target.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function (e) {
            this.setState({
                img1: [reader.result]
            })
        }.bind(this);
    }
    onCover(e){
        this.setState({cover:e.target.files[0]})
        var file = e.target.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function (e) {
            this.setState({
                cover1: [reader.result]
            })
        }.bind(this);
    }

    FormSubmit(event){
        const {token,onLogIn} = this.context;
        const formData = new FormData()
        formData.append('name',this.state.name)
        formData.append('phone',this.state.phone)
        formData.append('img',this.state.img)
        formData.append('cover',this.state.cover)
        formData.append('gender',this.state.gender)
        formData.append('birth',this.state.birth)
        formData.append('address',this.state.address)
        formData.append('des',this.state.des)
        formData.append('token',this.state.token)
        axios.post(ApiUrl.ProfileUpdate, formData)
            .then(res=>{
                if(res.data.success){
                    onLogIn(token)
                    this.componentDidMount()
                    toast.success(res.data.success, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    //Router.push(`/account`)
                }else{
                    toast.error(res.data.error, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
            })
            .catch(error=>{
                console.log(error)
            })

        event.preventDefault()

    }


    render() {
        if(this.state.loading==true){
            return (
                <ContentMain>
                    <SettingsTop/>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <SettingsTop/>
                    <div className="container-fluid p_settings">
                        <div className="row">
                            <div className="col-md-6 col-12">
                                <div className="form-group">
                                    <label htmlFor="inputAddress">Full Name</label>
                                    <input type="text" onChange={this.onName.bind(this)} value={this.state.name} className="form-control" id="inputAddress" placeholder="Full Name"/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="inputAddress">Phone</label>
                                    <input type="text" onChange={this.onPhone.bind(this)} value={this.state.phone} className="form-control" id="inputAddress" placeholder="Phone"/>
                                </div>
                                <div className="form-group ">
                                    <label htmlFor="inputState">Gender</label>
                                    <select id="inputState" onChange={this.onGender.bind(this)} className="form-control">
                                        <option selected value={this.state.gender}>{this.state.gender}</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Others">Others</option>
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="inputAddress">Address</label>
                                    <input type="text" onChange={this.onAddress.bind(this)} value={this.state.address} className="form-control" id="inputAddress" placeholder="Address"/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="inputAddress">Birth Date</label>
                                    <input type="text" onChange={this.onBirth.bind(this)} value={this.state.birth} className="form-control" id="inputAddress" placeholder=" DD/MM/YYYY"/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="inputAddress">About </label>
                                    <textarea rows="10" onChange={this.onDes.bind(this)} className="form-control" value={this.state.des}> </textarea>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="form-group">
                                    <label htmlFor="inputAddress">Profile Image</label>
                                    <input type="file" onChange={this.onImg.bind(this)} className="form-control" id="inputAddress" />
                                    <div className="col-sm-12">
                                        {this.state.img1?<img id="showImage" className="mt-3 mb-3" style={{height:'200px',width:'250px'}} src={this.state.img1} />:this.state.img ?<img className="mt-3 mb-3" style={{height:'200px',width:'250px'}} src={ApiUrl.photoUrl+this.state.img}/> :
                                            <img className="mt-3 mb-3" style={{height:'200px',width:'250px'}} src="/profile.png"/>
                                        }
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="inputAddress">Cover Image</label>
                                    <input type="file" onChange={this.onCover.bind(this)} className="form-control" id="inputAddress" />
                                    <div className="col-sm-12">
                                        {this.state.cover1?<img id="showImage" className="mt-3 mb-3" style={{height:'200px',width:'250px'}} src={this.state.cover1} />:this.state.cover ?<img className="mt-3 mb-3" style={{height:'200px',width:'250px'}} src={ApiUrl.photoUrl+this.state.cover}/> :
                                            <img className="mt-3 mb-3" style={{height:'200px',width:'250px'}} src="/profile.png"/>
                                        }
                                    </div>
                                </div>

                                <button className="btn btn-primary mt-3" onClick={this.FormSubmit.bind(this)} style={{float:'right',width:'100px'}}>SAVE</button>
                            </div>
                        </div>
                    </div>
                </ContentMain>
            );
        }
    }
}

export default Index;