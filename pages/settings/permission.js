import React, {Component} from 'react';
import SettingsTop from "../../component/profile/SettingsTop";
import ContentMain from "../../component/content/ContentMain";
import axios from "axios";
import ApiUrl from "../../component/AppApiUrl/ApiUrl";
import Router from "next/router";
import {toast} from "react-toastify";
import Loading from "../../component/loading/Loading";

class Permission extends Component {
    constructor() {
        super();
        this.state={
            s_status:'',f_status:'',token:'',loading:true
        }
    }

    componentDidMount() {
        const token = localStorage.token
        if(token){
            this.setState({token:token})
            const myFormData = new FormData()
            myFormData.append('token',token)
            const config = {
                headers : {'content-type':'multipart/form-data'}
            }
            axios.post(ApiUrl.ProfileAuth,myFormData,config)
                .then(res=>{
                    if(res.data.success){
                        this.setState({s_status:res.data.info[0]['s_status'],f_status:res.data.info[0]['f_status'],
                            token:token,loading:false})
                    }else{
                        console.log("Error")
                    }
                })
                .catch(error=>{
                    //localStorage.removeItem('token')
                    console.log(error)
                })
        }else{
            Router.push('/')
        }
    }

    onFollow(e){
        this.setState({f_status:e.target.value})
    }
    onSocial(e){
        this.setState({s_status:e.target.value})
    }


    FormSubmit(event){
        const formData = new FormData()
        formData.append('f_status',this.state.f_status)
        formData.append('s_status',this.state.s_status)
        formData.append('token',this.state.token)
        axios.post(ApiUrl.ProfileStatusUpdate, formData)
            .then(res=>{
                if(res.data.success){
                    this.componentDidMount()
                    toast.success(res.data.success, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    //Router.push(`/account`)
                }else{
                    toast.error(res.data.error, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
            })
            .catch(error=>{
                console.log(error)
            })

        event.preventDefault()

    }


    render() {
        if(this.state.loading==true){
            return (
                <ContentMain>
                    <SettingsTop/>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <SettingsTop/>
                    <div className="container-fluid p_settings">
                        <div className="row">
                            <div className="col-md-6 col-12">
                                <div className="form-group ">
                                    <label htmlFor="inputState">Social Permission</label>
                                    <select id="inputState" onChange={this.onSocial.bind(this)} className="form-control">
                                        <option selected value={this.state.s_status}>{this.state.s_status=="1"?"Public":"Private"}</option>
                                        <option value='1'>Public</option>
                                        <option value='0'>Private</option>
                                    </select>
                                </div>

                                <div className="form-group ">
                                    <label htmlFor="inputState">Follower Permission</label>
                                    <select id="inputState" onChange={this.onFollow.bind(this)} className="form-control">
                                        <option selected value={this.state.s_status}>{this.state.f_status=="1"?"Public":"Private"}</option>
                                        <option value='1'>Public</option>
                                        <option value='0'>Private</option>
                                    </select>
                                </div>

                                <button className="btn btn-primary mt-3" onClick={this.FormSubmit.bind(this)} style={{float:'right',width:'100px'}}>SAVE</button>
                            </div>
                            <div className="col-md-6 col-12">

                            </div>
                        </div>
                    </div>
                </ContentMain>
            );
        }
    }
}

export default Permission;