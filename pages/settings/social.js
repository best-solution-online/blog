import React, {Component} from 'react';
import ContentMain from "../../component/content/ContentMain";
import SettingsTop from "../../component/profile/SettingsTop";
import {DataContext} from "../../context/ContextApi";
import axios from "axios";
import ApiUrl from "../../component/AppApiUrl/ApiUrl";
import Router from "next/router";
import {toast} from "react-toastify";
import Loading from "../../component/loading/Loading";

class Social extends Component {
    static contextType = DataContext;
    constructor() {
        super();
        this.state={
            facebook:'',twitter:'',youtube:'',instagram:'',linkedin:'',token:'',loading:true
        }
    }

    componentDidMount() {
        const token = localStorage.token
        if(token){
            this.setState({token:token})
            const myFormData = new FormData()
            myFormData.append('token',token)
            const config = {
                headers : {'content-type':'multipart/form-data'}
            }
            axios.post(ApiUrl.ProfileAuth,myFormData,config)
                .then(res=>{
                    if(res.data.success){
                        this.setState({facebook:res.data.info[0]['facebook'],twitter:res.data.info[0]['twitter'],youtube:res.data.info[0]['youtube'],
                            instagram:res.data.info[0]['instagram'],linkedin:res.data.info[0]['linkedin'],token:token,loading:false})
                    }else{
                        console.log("Error")
                    }
                })
                .catch(error=>{
                    //localStorage.removeItem('token')
                    console.log(error)
                })
        }else{
            Router.push('/')
        }
    }

    onFacebook(e){
        this.setState({facebook:e.target.value})
    }
    onYoutube(e){
        this.setState({youtube:e.target.value})
    }
    onInstagram(e){
        this.setState({instagram:e.target.value})
    }
    onLinkedin(e){
        this.setState({linkedin:e.target.value})
    }
    onTwitter(e){
        this.setState({twitter:e.target.value})
    }

    FormSubmit(event){
        const formData = new FormData()
        formData.append('facebook',this.state.facebook)
        formData.append('twitter',this.state.twitter)
        formData.append('youtube',this.state.youtube)
        formData.append('linkedin',this.state.linkedin)
        formData.append('instagram',this.state.instagram)
        formData.append('token',this.state.token)
        axios.post(ApiUrl.ProfileSocialUpdate, formData)
            .then(res=>{
                if(res.data.success){
                    this.componentDidMount()
                    toast.success(res.data.success, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    //Router.push(`/account`)
                }else{
                    toast.error(res.data.error, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
            })
            .catch(error=>{
                console.log(error)
            })

        event.preventDefault()

    }


    render() {
        if(this.state.loading==true){
            return (
                <ContentMain>
                    <SettingsTop/>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <SettingsTop/>
                    <div className="container-fluid p_settings">
                        <div className="row">
                            <div className="col-md-6 col-12">
                                <div className="form-group">
                                    <label htmlFor="inputAddress">Facebook</label>
                                    <input type="text" onChange={this.onFacebook.bind(this)} value={this.state.facebook} className="form-control" id="inputAddress" placeholder="Facebook Link"/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="inputAddress">Youtube</label>
                                    <input type="text" onChange={this.onYoutube.bind(this)} value={this.state.youtube} className="form-control" id="inputAddress" placeholder="Youtube Link"/>
                                </div>

                                <div className="form-group">
                                    <label htmlFor="inputAddress">Twitter</label>
                                    <input type="text" onChange={this.onTwitter.bind(this)} value={this.state.twitter} className="form-control" id="inputAddress" placeholder="Twitter Link"/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="inputAddress">Linkedin</label>
                                    <input type="text" onChange={this.onLinkedin.bind(this)} value={this.state.linkedin} className="form-control" id="inputAddress" placeholder=" Linkedin Link"/>
                                </div>

                                <div className="form-group">
                                    <label htmlFor="inputAddress">Instagram</label>
                                    <input type="text" onChange={this.onInstagram.bind(this)} value={this.state.instagram} className="form-control" id="inputAddress" placeholder=" Instagram Link"/>
                                </div>

                                <button className="btn btn-primary mt-3" onClick={this.FormSubmit.bind(this)} style={{float:'right',width:'100px'}}>SAVE</button>
                            </div>
                            <div className="col-md-6 col-12">

                            </div>
                        </div>
                    </div>
                </ContentMain>
            );
        }

    }
}

export default Social;