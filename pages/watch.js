import Moment from 'react-moment';
import React, {Component} from 'react';
import ContentMain from "../component/content/ContentMain";
import Head from "next/head";
import Router from "next/router";
import moment from "moment";
import Link from "next/link";
import axios from "axios";
import ApiUrl from "../component/AppApiUrl/ApiUrl";
import Loading from "../component/loading/Loading";
import {DataContext} from "../context/ContextApi";
import jwtDecode from "jwt-decode";
import {toast} from "react-toastify";


class Watch extends Component {
    static contextType = DataContext;
    static getInitialProps({query}){
        return (
            {query}
        )
    }

    constructor() {
        super();
        this.state={
            comment_btn:true,
            comment:'',reply:'',report:'',f:'0',
            reply_btn:0,
            reply_reply_btn:0,
            comment_id:'',role:'',comment_edit_id:'',comment_edit:'',reply_edit:'',reply_edit_id:'',
            reply_id:'', cid:'',path:'',likes:[],dislike:[],likecount:'0',dislikecount:'0',
            token:'',title:'',body:'',tags:'',category:'',meta:'',views:'',myid:'',com_status:'',f_status:'',
            created_at:'',u_name:'',u_slug:'',u_img:'',u_follow:'',channel:'',followers:'0',commentData:[],
            status:'',img:'',img1:'',video:'',catList:[],pid:'',loading:true
        }
    }

    componentDidMount() {
        const token = localStorage.token
        if(token){
            const decoded = jwtDecode(token)
            this.setState({token:token,myid:decoded.uid,role:decoded.role})
        }
        this.setState({path:window.location.href})
        console.log(window.location.href)
        const formData = new FormData()
        formData.append('slug',this.props.query.p)
        axios.post(ApiUrl.PostSlug,formData)
            .then(res=>{
                console.log(res)
                this.setState({pid:res.data.post[0]['id'],category:res.data.post[0]['category'],
                    title:res.data.post[0]['title'],body:res.data.post[0]['body'],tags:res.data.post[0]['tags'],
                    f_status:res.data.post[0]['f_status'],
                    meta:res.data.post[0]['meta'],status:res.data.post[0]['status'],img:res.data.post[0]['img'],commentData:res.data.comments,
                    views:res.data.post[0]['views'],created_at:res.data.post[0]['created_at'],u_name:res.data.post[0]['u_name'],
                    u_img:res.data.post[0]['u_img'],u_follow:res.data.post[0]['u_followers'],channel:res.data.post[0]['channel'],
                    u_slug:res.data.post[0]['u_slug'],com_status:res.data.post[0]['com_status'],likes:res.data.like,dislike:res.data.dislike
                })

                const formData2 = new FormData()
                formData2.append('channel',res.data.post[0]['channel'])
                formData2.append('pid',res.data.post[0]['id'])
                formData2.append('token',token)
                axios.post(ApiUrl.FollowCheck, formData2)
                    .then(res=>{
                        console.log(res)
                        if(res.data.success){
                            this.setState({followers:'1', loading:false})
                        }else{
                            this.setState({followers:'0', loading:false})
                        }
                    })
                    .catch(error=>{
                        console.log(error)
                        this.setState({loading:false})
                    })


                axios.post(ApiUrl.PostLikeDislikeCheck, formData2)
                    .then(res=>{
                        console.log(res)
                        if(res.data.like){
                            this.setState({likecount:'1', loading:false})
                        }else if(res.data.dislike){
                            this.setState({dislikecount:'1', loading:false})
                        }else{
                            console.log(error)
                        }
                    })
                    .catch(error=>{
                        console.log(error)
                        this.setState({loading:false})
                    })

                axios.post(ApiUrl.FavoriteCheck, formData2)
                    .then(res=>{
                        console.log(res)
                        if(res.data.f =='1'){
                            this.setState({f:'1', loading:false})
                        }else{
                            this.setState({f:'0', loading:false})
                        }
                    })
                    .catch(error=>{
                        console.log(error)
                        this.setState({loading:false})
                    })


            })
            .catch(error=>{
                console.log(error)
                this.setState({loading:false})
            })
    }

    report=(e)=>{
        this.setState({report:e.target.value})
    }

    ReportSubmit=(event)=>{
        if(this.state.report !="" &&  this.state.token !="" && this.state.pid !=""){
            const formData = new FormData()
            formData.append('report',this.state.report)
            formData.append('pid',this.state.pid)
            formData.append('channel',this.state.channel)
            formData.append('token',this.state.token)
            axios.post(ApiUrl.ReportCreate, formData)
                .then(res=>{
                    if(res.data.success){
                        this.setState({report:''})
                        toast.success(res.data.success, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }else{
                        toast.error(res.data.error, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }
                })
                .catch(error=>{
                    console.log(error)
                })
        }else{
            toast.error("Login is required", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
        event.preventDefault()
    }

    FavoriteSubmit=(event)=>{
        if(this.state.token !="" && this.state.pid !=""){
            const formData = new FormData()
            formData.append('report',this.state.report)
            formData.append('pid',this.state.pid)
            formData.append('channel',this.state.channel)
            formData.append('token',this.state.token)
            axios.post(ApiUrl.FavoriteCreate, formData)
                .then(res=>{
                    this.setState({f:res.data.f})
                    if(res.data.success){
                        toast.success(res.data.success, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }else{
                        toast.error(res.data.error, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }
                })
                .catch(error=>{
                    console.log(error)
                })
        }else{
            toast.error("Login is required", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
        event.preventDefault()
    }

    commenteditclick=(id,comment)=>{
        this.setState({comment_edit_id:id,comment_edit:comment})
    }
    oncomments=(e)=>{
        this.setState({comment_edit:e.target.value})
    }
    replyeditclick=(id,reply)=>{
        this.setState({reply_edit_id:id,reply_edit:reply})
    }
    onreplys=(e)=>{
        this.setState({reply_edit:e.target.value})
    }

    EditReplySubmit=(event)=>{
        if(this.state.reply_edit !="" &&  this.state.token !=""){
            const formData = new FormData()
            formData.append('reply',this.state.reply_edit)
            formData.append('id',this.state.reply_edit_id)
            formData.append('token',this.state.token)
            axios.post(ApiUrl.ReplyUpdate, formData)
                .then(res=>{
                    if(res.data.success){
                        this.componentDidMount()
                        this.setState({reply_edit:'',reply_edit_id:''})
                        toast.success(res.data.success, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }else{
                        toast.error(res.data.error, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }
                })
                .catch(error=>{
                    console.log(error)
                })
        }
        event.preventDefault()
    }

    EditCommentSubmit=(event)=>{
        if(this.state.comment_edit !="" &&  this.state.token !=""){
            const formData = new FormData()
            formData.append('comment',this.state.comment_edit)
            formData.append('id',this.state.comment_edit_id)
            formData.append('token',this.state.token)
            axios.post(ApiUrl.CommentUpdate, formData)
                .then(res=>{
                    if(res.data.success){
                        this.componentDidMount()
                        this.setState({comment_edit:'',comment_edit_id:''})
                        toast.success(res.data.success, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }else{
                        toast.error(res.data.error, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }
                })
                .catch(error=>{
                    console.log(error)
                })
        }
        event.preventDefault()
    }

    onFollow=()=>{
        const {token} = this.context;
      if(token == ""){
          toast.error("Login is required", {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
          });
      }else{
          const formData2 = new FormData()
          formData2.append('channel',this.state.channel)
          formData2.append('token',token)
          axios.post(ApiUrl.FollowCreate, formData2)
              .then(res=>{
                  //this.componentDidMount()
                  console.log(res)
                  if(res.data.success){
                      console.log(res.data.success)
                      this.setState({followers:res.data.success})
                      this.componentDidMount()
                  }else{
                      console.log("Error Follow")
                  }
              })
              .catch(error=>{
                  console.log(error)
              })
      }
    }

    onLike=()=>{
        const {token} = this.context;
        if(token == ""){
            toast.error("Login is required", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }else{
            const formData2 = new FormData()
            formData2.append('pid',this.state.pid)
            formData2.append('token',token)
            axios.post(ApiUrl.PostLike, formData2)
                .then(res=>{
                    console.log(res)
                    if(res.data.success == "1"){
                        console.log(res.data.success)
                        this.setState({likecount:"1",dislikecount:'0'})
                        this.componentDidMount()
                    }else{
                        this.setState({likecount:"0",dislikecount:'0'})
                        this.componentDidMount()
                    }
                })
                .catch(error=>{
                    console.log(error)
                })
        }
    }

    onDisLike=()=>{
        const {token} = this.context;
        if(token == ""){
            toast.error("Login is required", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }else{
            const formData2 = new FormData()
            formData2.append('pid',this.state.pid)
            formData2.append('token',token)
            axios.post(ApiUrl.PostDislike, formData2)
                .then(res=>{
                    console.log(res)
                    if(res.data.success == "1"){
                        console.log(res.data.success)
                        this.setState({dislikecount:'1',likecount:'0'})
                        this.componentDidMount()
                    }else{
                        this.setState({likecount:"0",dislikecount:'0'})
                        this.componentDidMount()
                    }
                })
                .catch(error=>{
                    console.log(error)
                })
        }
    }

    onHideCbtn=()=>{
        this.setState({comment_btn:false})
    }

    oncomment=(e)=>{
        this.setState({comment_btn:true,comment:e.target.value})
    }

    SubmitComment=(event)=>{
        if(this.state.comment !="" && this.state.pid !="" && this.state.token !=""){
            const formData = new FormData()
            formData.append('comment',this.state.comment)
            formData.append('pid',this.state.pid)
            formData.append('token',this.state.token)
            axios.post(ApiUrl.CommentCreate, formData)
                .then(res=>{
                    if(res.data.success){
                        this.componentDidMount()
                        this.setState({comment:''})
                        toast.success(res.data.success, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                        //Router.push(`/account`)
                    }else{
                        toast.error(res.data.error, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }
                })
                .catch(error=>{
                    console.log(error)
                })
        }
        event.preventDefault()
    }

    onreply=(e)=>{
        this.setState({reply:e.target.value})
    }

    oncopy=() => {
        navigator.clipboard.writeText(this.state.path)
        toast.success("Share Link successfully copied", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }
    SubmitReply=(event)=>{
        if(this.state.reply !="" && this.state.pid !="" && this.state.cid !="" && this.state.token !=""){
            const formData = new FormData()
            formData.append('reply',this.state.reply)
            formData.append('pid',this.state.pid)
            formData.append('cid',this.state.cid)
            formData.append('token',this.state.token)
            axios.post(ApiUrl.ReplyCreate, formData)
                .then(res=>{
                    if(res.data.success){
                        this.componentDidMount()
                        this.setState({reply:'',reply_btn:0,reply_reply_btn:0})
                        toast.success(res.data.success, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }else{
                        toast.error(res.data.error, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }
                })
                .catch(error=>{
                    console.log(error)
                })
        }
        event.preventDefault()
    }

    onShowCommentReplyBTN=(e)=>{
        this.setState({reply_btn:1,comment_id:e,cid:e})
    }
    onHideCommentReplyBTN=(e)=>{
        this.setState({reply_btn:0,comment_id:e})
    }

    onShowReplyReplyBTN=(id,cid)=>{
        this.setState({reply_reply_btn:1,reply_id:id,cid:cid})
    }
    onHideReplyReplyBTN=(id)=>{
        this.setState({reply_reply_btn:0,reply_id:id})
    }

    CommentDelete=(id)=>{
            const formData = new FormData()
            formData.append('id',id)
            formData.append('token',this.state.token)
            axios.post(ApiUrl.CommentDelete, formData)
                .then(res=>{
                    if(res.data.success){
                        this.componentDidMount()
                        toast.success(res.data.success, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }else{
                        toast.error(res.data.error, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }
                })
                .catch(error=>{
                    console.log(error)
                })

    }

    ReplyDelete=(id)=>{
        const formData = new FormData()
        formData.append('id',id)
        formData.append('token',this.state.token)
        axios.post(ApiUrl.ReplyDelete, formData)
            .then(res=>{
                if(res.data.success){
                    this.componentDidMount()
                    toast.success(res.data.success, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }else{
                    toast.error(res.data.error, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
            })
            .catch(error=>{
                console.log(error)
            })

    }

    render() {
        const {img,logo,icon,title,footer} = this.context;

         const comment = this.state.commentData.map(res=>{
             return(
                 <>
                     <div className="col-md-1 col-2 pimgsection">
                         <Link href={"/channel/"+res.u_slug}>
                             <a>{res.u_img ? <img className="puserpic"
                                                  src={ApiUrl.photoUrl+res.u_img}/>:
                                 <img className="puserpic"
                                      src="/profile.png"/>
                             }</a>
                         </Link>
                     </div>
                     <div className="col-md-11 col-10 pt-3">
                         <Link href={"/channel/"+res.u_slug}><a>
                             <p>{res.u_name} {<Moment fromNow>{res.date}</Moment>}</p>
                         </a></Link>
                         <p>
                             {res.comment}
                         </p>

                         {this.state.myid == res.uid  ?
                             <a className="comment_icon" onClick={this.commenteditclick.bind(this,res.id,res.comment)}
                                data-toggle="modal" data-target="#commentModal" type="button">Edit</a>:''}
                         <div className="modal fade" id="commentModal" tabIndex="-1" role="dialog"
                              aria-labelledby="exampleModalLabel" aria-hidden="true">
                             <div className="modal-dialog" role="document">
                                 <div className="modal-content">
                                     <div className="modal-header">
                                         <h5 className="modal-title" id="exampleModalLabel">Edit Comment</h5>
                                     </div>
                                     <div className="form-group m-2">
                                         <textarea rows="6" onChange={this.oncomments} value={this.state.comment_edit} className="form-control"></textarea>
                                     </div>
                                     <div className="modal-footer">
                                         <button type="button" className="btn btn-secondary"
                                                 data-dismiss="modal">Cancel
                                         </button>
                                         <button type="button" onClick={this.EditCommentSubmit} data-dismiss="modal"
                                                 className="btn btn-primary">Save </button>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         {this.state.myid == res.uid || this.state.channel == this.state.myid || this.state.role=="Admin"?
                             <a className="comment_icon" onClick={this.CommentDelete.bind(this, res.id)} type="button">Delete</a> :''}
                         <a type="button" value="1"
                            onClick={this.onShowCommentReplyBTN.bind(this, res.id)}
                            className="comment_icon">REPLY</a>
                         {this.state.reply_btn == 1 && this.state.comment_id == res.id ?
                             <div className="row">
                                 <div className="col-md-1 col-2 pimgsection">
                                         <a>{img ? <img className="puserpic"
                                                        src={ApiUrl.photoUrl + img}/> :
                                             <img className="puserpic"
                                                  src="/profile.png"/>
                                         }</a>
                                 </div>
                                 <div className="col-md-11 col-10 pt-1 ">
                                     <input placeholder="Add a public reply" onChange={this.onreply}
                                            value={this.state.reply}  className="form-control comment_input"/>
                                     <div className="float-end mt-3">
                                         <a type="button"
                                            onClick={this.onHideCommentReplyBTN.bind(this, res.id)}
                                            className="">CANCEL</a>
                                         <button onClick={this.SubmitReply} className="btn btn-success comment_btn">REPLY
                                         </button>
                                     </div>
                                 </div>
                             </div>
                             : ""}
                         {/*Reply Input and List */}
                         <div className="row" style={{paddingRight: '0px'}}>

                             {/*<a type="button" className="col-md-12 col-12 pt-1 text-primary"><i
                                                className="fas fa-sort-down mr-5"></i> <span> View 4 replies</span></a>*/}

                             { res.replies.map(result=>{
                             return(
                             <>
                             <div className="col-md-1 col-2 pimgsection">
                             <Link href={"/channel/"+result.u_slug}>
                             <a>{result.u_img ? <img className="puserpic"
                                                     src={ApiUrl.photoUrl+result.u_img}/>
                                                     :
                                 <img className="puserpic"
                                      src="/profile.png"/>
                             }</a>
                             </Link>
                             </div>
                             <div className="col-md-11 col-10 pt-3">
                                 <Link href={"/channel/"+result.u_slug}><a>
                                     <p>{result.u_name} {<Moment fromNow>{res.date}</Moment>}</p>
                                 </a></Link>
                             <p>
                                 {result.reply}
                             </p>
                                 {this.state.myid == result.uid ?
                                     <a data-toggle="modal" onClick={this.replyeditclick.bind(this,result.id,result.reply)} data-target="#replyModal" className="comment_icon" type="button">Edit</a>:''}
                                 <div className="modal fade" id="replyModal" tabIndex="-1" role="dialog"
                                      aria-labelledby="exampleModalLabel" aria-hidden="true">
                                     <div className="modal-dialog" role="document">
                                         <div className="modal-content">
                                             <div className="modal-header">
                                                 <h5 className="modal-title" id="exampleModalLabel">Edit Reply</h5>
                                             </div>
                                             <div className="form-group m-2">
                                                 <textarea rows="6" onChange={this.onreplys} value={this.state.reply_edit} className="form-control"></textarea>
                                             </div>
                                             <div className="modal-footer">
                                                 <button type="button" className="btn btn-secondary"
                                                         data-dismiss="modal">Cancel
                                                 </button>
                                                 <button type="button" onClick={this.EditReplySubmit} data-dismiss="modal"
                                                         className="btn btn-primary">Save </button>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 {this.state.myid == result.uid || this.state.channel == this.state.myid || this.state.role=="Admin" ?
                                     <a className="comment_icon" onClick={this.ReplyDelete.bind(this, result.id)} type="button">Delete</a> :''}

                             <a type="button" onClick={this.onShowReplyReplyBTN.bind(this, result.id,res.id)}
                             className="comment_icon">REPLY</a>
                             {this.state.reply_reply_btn == 1 && this.state.reply_id == result.id ?
                                 <div className="row">
                                     <div className="col-md-1 col-2 pimgsection rrpic">
                                             <a>{img ? <img className="puserpic"
                                                            src={ApiUrl.photoUrl + img}/> :
                                                 <img className="puserpic"
                                                      src="/profile.png"/>
                                             }</a>
                                     </div>
                                     <div className="col-md-11 col-10 pt-1 ">
                                         <input placeholder="Add a public reply" onChange={this.onreply}
                                                value={this.state.reply} className="form-control comment_input"/>
                                         <div className="float-end mt-3">
                                             <a type="button"
                                                onClick={this.onHideReplyReplyBTN.bind(this, result.id)}
                                                className="">CANCEL</a>
                                             <button onClick={this.SubmitReply}
                                                 className="btn btn-success comment_btn">REPLY
                                             </button>
                                         </div>
                                     </div>
                                 </div>
                                 : ""}

                             </div>
                             </>
                             )
                         })
                             }


                         </div>
                         {/*Reply Input and List Close */}
                     </div>
                 </>
             )
         })

        if(this.state.loading==true){
            return (
                <ContentMain>
                    <title> Data not found yet </title>
                    <Loading/>
                </ContentMain>
            )
        }else{
            return (
                <ContentMain>
                    <Head>
                        <link rel="apple-touch-icon" href={ApiUrl.photoUrl+logo}/>
                            <link rel="shortcut icon" href={ApiUrl.photoUrl+icon}/>
                        <meta property="og:type" content="article" />
                        <meta property="og:title" content={this.state.title} />
                        <meta property="og:description" content={this.state.body} />
                        <meta property="og:site_name" content={footer} />
                        <meta property="og:image" content={ApiUrl.photoUrl+this.state.img} />
                        <meta property="og:url" content={this.state.path} />
                        <meta property="article:tag" content={this.state.title} />
                        <meta name="twitter:card" content="summary_large_image" />
                        <meta name="twitter:title" content={this.state.title} />
                        <meta name="twitter:description" content={this.state.body} />
                        <meta name="twitter:image" content={ApiUrl.photoUrl+this.state.img} />
                        <meta name="description" content={this.state.body} />
                        <meta name="keywords" content={this.state.tags}/>
                        <meta name="author" content={title}/>
                        <title>{this.state.title}</title>
                    </Head>
                    <div className="container-fluid">
                        <div className="row watch">
                            <div className="col-md-12 col-12 p-3">
                                {this.state.img ? <img className="d-block w-100 single_p_img"
                                                       src={ApiUrl.photoUrl+this.state.img} />:
                                    <img className="d-block w-100 single_p_img"
                                         src="/noimage.png" />
                                }
                                <h4 className="pt-3 ">
                                    {this.state.title}

                                </h4>
                                <div className="row mt-3  ">
                                    <div className="col-md-4 col-12">
                                        <p className="pt-2 views">{this.state.views} views. {moment(this.state.created_at).format("MMM Do YY")}</p>
                                    </div>
                                    <div className="col-md-8 col-12 sicon">
                                        <ul>
                                            <li>
                                                <a onClick={this.onLike} className={this.state.likecount=="1" ?"text-primary":""}>
                                                    <i className="far fa-thumbs-up"></i> {this.state.likes.length == "0" ? "" : this.state.likes.length}</a>
                                            </li>
                                            <li>
                                                <a onClick={this.onDisLike} className={this.state.dislikecount=="1" ?"text-primary":""}>
                                                    <i className="far fa-thumbs-down "></i>
                                                    {this.state.dislike.length == "0" ? "" : this.state.dislike.length}</a>
                                            </li>
                                            <li>
                                                <a onClick={this.oncopy} title={"Copy Share Link"}>
                                                    <i className="far fa-share"></i> SHARE</a>
                                            </li>
                                            <li>
                                                <a onClick={this.FavoriteSubmit} className={this.state.f=="1" ?"text-primary":""}>
                                                    <i className="far fa-plus-square"></i> SAVE</a>
                                            </li>
                                            <li>
                                                <a type="button"  data-toggle="modal" data-target="#exampleModal"><i className="fas fa-ellipsis-h"></i></a>
                                                <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog"
                                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div className="modal-dialog" role="document">
                                                        <div className="modal-content">
                                                            <div className="modal-header">
                                                                <h5 className="modal-title" id="exampleModalLabel">Report</h5>
                                                            </div>
                                                            <div className="form-group m-2">
                                                                <textarea rows="6" value={this.state.report} onChange={this.report}  className="form-control"></textarea>
                                                            </div>
                                                            <div className="modal-footer">
                                                                <button type="button" className="btn btn-danger"
                                                                        data-dismiss="modal">Cancel
                                                                </button>
                                                                <button type="button"  data-dismiss="modal"
                                                                      onClick={this.ReportSubmit}  className="btn btn-primary"> Submit </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <hr className="col-md-12 col-12" />

                                <div className="row">
                                    <div className="col-md-6 col-9">
                                        <div className="row">
                                            <div className="col-md-1 col-2">
                                                <Link href={"/channel/"+this.state.u_slug}>
                                                    <a>{this.state.u_img ? <img className="puserpic"
                                                                                src={ApiUrl.photoUrl+this.state.u_img}/>:
                                                        <img className="puserpic"
                                                             src="/profile.png"/>
                                                    }</a>
                                                </Link>
                                            </div>
                                            <div className="col-md-9 mt-2 col-10 ml-1">
                                                <p className="pt-2 views"><Link href={"/channel/"+this.state.u_slug}>{this.state.u_name}</Link></p>
                                                {this.state.f_status=="1" || this.state.channel == this.state.myid ?
                                                    <p className="pt-1 views">{this.state.u_follow} followers</p>:
                                                    <p className="pt-1 views"> Hidden followers</p>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-3 pt-3 ">
                                        {/*Followers Btn*/}

                                        {this.state.channel==this.state.myid ? "":
                                            this.state.followers == "1" ? <button onClick={this.onFollow} className="btn btn-white followbtn float-end" >FOLLOWED</button>:
                                                <button onClick={this.onFollow} className="btn btn-danger followbtnactive float-end" >FOLLOW</button>
                                        }

                                    </div>
                                </div>
                                <div className="row " style={{marginLeft:'30px',marginTop:'20px'}}>
                                    <div className="col-md-12">
                                        <p>
                                            {this.state.body}
                                        </p>
                                    </div>
                                </div>

                                {this.state.com_status == "On" ? <>
                                    <hr className="col-md-12 col-12 "/>
                                    <p> {this.state.commentData.length} Comments</p>

                                    <div className="row">
                                        {/*Comment Input */}
                                        <div className="col-md-1 col-lg-1 col-2 pimgsection pimginput">
                                            <Link href="/info/ansarul">
                                                <a>{img ? <img className="puserpic"
                                                               src={ApiUrl.photoUrl + img}/> :
                                                    <img className="puserpic"
                                                         src="/profile.png"/>
                                                }</a>
                                            </Link>
                                        </div>
                                        <div className="col-md-11 col-10 pt-1 col-lg-11">
                                            <input onChange={this.oncomment} placeholder="Add a public comment"
                                                 value={this.state.comment}  className="form-control comment_input"/>
                                            {this.state.comment_btn == true ? <div className="float-end mt-3">
                                                <a type="button" onClick={this.onHideCbtn} className="">CANCEL</a>
                                                <button onClick={this.SubmitComment} className="btn btn-success comment_btn">COMMENT</button>
                                            </div> : ""}
                                        </div>

                                        {/*Comment Input Close */}

                                        {/*Comment List Start */}
                                        {comment}
                                        {/* Comment List Close */}
                                    </div>
                                </>
                                    :<h4 className="text-center p-5">Comment off</h4>}

                            </div>
                        </div>
                    </div>
                </ContentMain>
            );
        }
    }
}

export default Watch;
